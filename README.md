# WARNING
__Videos or pictures on this site may potentially trigger seizures for people with photosensitive epilepsy__

# Releases
You can find all releases [HERE](https://gitlab.com/YannikW/whooplights/tags).

# What is this?

__whoopLights__ is an open source project about LED gates for FPV racing.  
It is completly network based and synchonized. This unique features make it possible to create effects with multiple gates without wiring multiple gates to one controller or other complicated solutions.

The whoopLights project consists of two (or three) parts.  
The __Controller__, the __App__, and - but that's not really specific to the whoopLights prject - the __Gate__.

But - do you really know what whoopLights is now? Not really, do you!?  
So let some pictures and videos talk for me with some examples.

# Examples
<img src="https://media.giphy.com/media/f9ShyKj22a21X1dLPS/giphy.gif"  width="420">  
<img src="https://media.giphy.com/media/Lp57UacUjZjFNlwxIe/giphy.gif"  width="420">  
<img src="https://media.giphy.com/media/bqYNFSP6s5Fg4jTtr3/giphy.gif"  width="420">  
<img src="https://media.giphy.com/media/n5HJqHLTlC7p8r3pmR/giphy.gif"  width="420">  

# Controller
- __ESP8266 / Arduino based__
- __WS2811 or WS2812(b) compatible__
- __Mesh network synchronized__   
    All controller generate a single network for the app.
- __Easy configuration__  
    No changes in arduino code required.
- __Easy to build__  
    No programming knowledge or tool installations required.
- __Cheap__  
    A complete gate under 10$ for the controller and leds.
- __3S - 4S LiPo input__

<img src="docs/pics/controller_top.jpg"  width="380">
<img src="docs/pics/controller_bottom.jpg"  width="430">

# App
- __Cross-platform compatible__ (For now only Android available)  
    Written in React / Javascript it can run on any device.
- __Easy to use__  
    The UI is reduced to a minimum. One self explaining page - no nested menus or complicated settings.

![app_connect_screen](docs/pics/app_connect_screen_readme.jpg)
![app_main_screen](docs/pics/app_main_screen_readme.jpg)

# Gate
A word in advance: The choose of the gate is totally independant to the whoopLights controller or app.  
You can choose whatever you want. You can use whoopLights for Tiny Whoops or of cause for bigger quads / gates.  

But: whoopLights - I guess you noticed from the name - is designed for tiny whoops. You can configure everything to match bigger scales, but in my opinion it's best for whoops.

Of cause whoopLights isn't limited to gates! Most default effects will work fine for flags, arrows, tunnels or whatever you want to.

To get the best experience for whoopLights I designed my own 3d printed gates. They are available in different sizes for different strip length. Because that's not fully related to whoopLights all details about the gates can be found at Thingiverse: https://www.thingiverse.com/thing:3169911

<img src="docs/pics/gate.jpg"  width="640">  
<br>
<img src="docs/pics/gate_led_strip.jpg"  width="320">
<img src="docs/pics/gate_suction_cup.jpg"  width="320">

# How to build
For a complete tutorial about how to build and programm a gate with controller, see [here](docs/How_to_build.md).

# Developer
Feel free to become a part of the developer team.  
You can help with optimizing code, adding features, create new effects or simple correct my bad english translated documentation :P

For software development you can find a list of ToDos [here](docs/ToDo.md).

If you have a brilliant idea for a new effect feel free to add in yourself and merge it back into this project.
You can find a (more or less) detailed description of how to add an effect [here](docs/Add_Effect.md).

# Troubleshooting
## Android App
### App doesn't connect
Please follow these steps to solve:

1. Make sure you're connected to the whoopLights wifi.  
The app should try to connect automatic to this network on start. If this fails you can try yourself (SSID: "whoopLights", pass: "12345678")
2. If there is a prompt with _no internet_ after making the wifi connection, tap on it and accept it (wording is different between android versions and manufacturers)
3. _ToDo: ping www.whooplights.esp_