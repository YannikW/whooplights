# Table of Content

- [What is this](#what-is-this)
- [Version](#version)
- [Settings](#settings)
- [Effects](#effects)

# What is this
All settings are saved is the ESP8266 SPIFFS.  
This filesystem allows us to save the configuration in JSON format which ist much easier then storing some bytes in the EEPROM.

After an update the new software will try to load the old values. In the new software is a minimum version defined for each parameter. Only if the version in the config file ist equal or greater this minimum version the value is loaded. If not the default value will be used.  
This can happen if the parameter for an effect has changed and a incompatible with the last version.

There are two different config, on is the setttings-config and one the effects-config.
Keys are ordered alphabetical.

# Version
Following configs are default configurations in version `0.1.0`.

# Settings
This is the current default settings configuration, saved in `defaults.h`.  
The user configuration will be saved in `settings.json` in SPIFFS (not part of source code - will be created only inside ESP).

```json
{
    "alias": {
        "version": [0,1,0],
        "value": ""
    },
    "numLeds": {
        "version": [0,1,0],
        "value": 20
    },
    "voltageScale": {
        "version": [0,1,0],
        "value": 17661
    },
    "warningCellVoltage": {
        "version": [0,1,0],
        "value": 3650
    },
    "brightness": {
        "version": [0,1,0],
        "value": 255
    }
}
```

# Effects
This is the current default effect configuration, saved in `defaults.h`.  
The user configuration will be saved in `effects.json` in SPIFFS (not part of source code - will be created only inside ESP).

Note: in the effects-array is a name key - this wil not be used because it's hardcoded is software.

```json
{
    "activeEffectId": {
        "version": [0,1,0],
        "value": 0
    },
    "effects": [
        {
            "name": "Rainbow",
            "version": [0,1,0],
            "color": {
                "primary": [0,0,0],
                "secondary": [0,0,0]
            },
            "para": [14,2,0,0,0]
        },  
        {
            "name": "Fire",
            "version": [0,1,0],
            "color": {
                "primary": [255,96,12],
                "secondary": [0,0,0]
            },
            "para": [30,160,70,0,0]
        },
        {
            "name": "Tunnel",
            "version": [0,1,0],
            "color": {
                "primary": [255,0,40],
                "secondary": [0,30,30]
            },
            "para": [200,3,1,1,0]
        },
        {
            "name": "Larson Scanner",
            "version": [0,1,0],
            "color": {
                "primary": [255,0,0],
                "secondary": [40,40,40]
            },
            "para": [30,5,0,0,0]
        },
        {
            "name": "Snake",
            "version": [0,1,0],
            "color": {
                "primary": [255,70,0],
                "secondary": [0,70,70]
            },
            "para": [50,0,0,0,0]
        },
        {
            "name": "Theater Chase",
            "version": [0,1,0],
            "color": {
                "primary": [77,0,255],
                "secondary": [200,200,0]
            },
            "para": [50,2,3,0,0]
        },
        {
            "name": "Strobe",
            "version": [0,1,0],
            "color": {
                "primary": [255,255,255],
                "secondary": [0,0,0]
            },
            "para": [25,0,0,0,0]
        },
        {
            "name": "Police",
            "version": [0,1,0],
            "color": {
                "primary": [255,0,0],
                "secondary": [0,0,255]
            },
            "para": [50,2,0,0,0]
        }
    ]
}
```