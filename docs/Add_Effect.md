# Table of Content

- [What is this](#whatisthis)
- [For who is this](#forwhoisthis)
- [Arduino Code](#arduinocode)
- [App Code](#appcode)
- [Documentation](#documentation)

# What is this

This files describes what you have to add or change to add an effect to both, the ESP8266 hardware and the app.  
This _tutorial_ is dived in [Arduino Code](#arduinocode) and [App Code](#appcode). In both categories are sub-sections for each code file.  
Last, but not least, there is a section about the [Documentation](#documentation), so other users or developers can lookup what you effect is and how it's configured.

# For who is this

To be honest - The idea of making a doc how to add an effect was primary for myself to remember what steps has to be done (If you're a dev you know what I'm talking about - two weeks later you forget everything :P ).

But nevertheless I will describe what I'm doing here. So if you have some experience in Arduino, C++, classes, functions, Neopixel leds, ... you will be able to understand what is needed to an an effect. It's not that complicated but it is definitly more advanced then arduino hello world or blink program ;)

If you have no idea what the above keywords mean this is not a good task for you - but maybe a challenge? ;)  
You can also open an issue / feature request for your effect. This should contain a description, a video or some example code for the effect.  
Then others (or I) can try to implement it :) 

# Arduino Code

Note: My code is divides in sections beginning with a comment header.  
I will show you the header and the parts where something has to be inserted or changed.  
If i leave out some code between comment header and part to insert I will mark it with four dots in a bracket `[....]`.  

## Effects.h
```c++
/*#######################################################
##                 Function prototypes                 ##
#######################################################*/
[....]
//Effects
void rainbow();
RgbColor wheel(uint8_t wheelPos);
void fire();
```
Add your effect-function prototype here, returning `void` without parameter:
```c++
void yourEffectFunction();
```

---

```c++
/*#######################################################
##              Default effect parameters              ##
#######################################################*/
const effectCallback_t effectCallbacks[] = {
    {"Rainbow", rainbow},
    {"Fire", fire},
};
```

Add an entrie to the __END__ (this is important! don't insert it at beginning or in the middle) of the array `effectCallbacks[]`, with following pattern:
```c++
{"Your effect name", yourEffectFunction}
```

## Effects.cpp

```c++
/*#######################################################
##                  Effect Functions                   ##
#######################################################*/

void rainbow() {
    [....]
}

void fire() {

}
```

In the `Effect Functions` part you have to add your function which actually produces the effect.  
This is the most important and tricky (of cause this depends on the complexity of your effect) part.

There are some importent things to take care of here, compared to other project with neopixel. 

__ONE function call must produce only ONE frame of the effect!__

For example take the Rainbow effect (which is an adaption of an original Neopixel example).  
In the original example one function call cycles through the whole rainbow (at space AND time).  
There is a for loop for to cycle through each led in the strip (this is okay) and an outer for loop to cycle through 255 steps in time, each with a `delay()` at the end (this is forbidden!).

So in conclusion: __You must NOT use delay inside this function!__

For better understanding just look at the present effect function.

To get your effect working properly you maybe need some variables, do not use global variables!  
Following, existing variables can be used:
```c++
/**
 * This returns a counter variable for this effect (since it was enabled).
 * On first time the function executes this will return 1.
 * It will be resetted every time the effect reaches it's iterations (see later)
 * and will be reenabled.
 */
effectTasks[activeEffectId].getRunCounter();
```
```c++
/**
 * This returns the number of pixels your strip has.
 * Note: If this return 20, your indices for the pixels are 0-19.
 */
strip->PixelCount();
```
```c++
/**
 * This returns a random number between 0 and max-1.
 */
ESP8266TrueRandom.random(max);
```
```c++
/**
 * This returns a - b, but with underflow protection.
 * If a > b, this returns a - b
 * If a <= b, this returns 0 
 */
sub_wo_underflow(a,b);
```
```c++
/**
 * This returns a - b, but with underflow protection.
 * If a > b, this returns a - b
 * If a <= b, this returns 0 
 */
sub_wo_underflow(a,b);
```

---

Also you can access the effect colors and parameter you defined (we will see late how you can define these default values for these parameters).

```c++
/**
 * This are the RgbColor objects.
 *
 * These variables are not protected against writing,
 * but NEVER write new values to these variables!
 * Just read them to a local variable which you can manipulate if you want.
 */
effects[activeEffectId].color[0];   // primary color
effects[activeEffectId].color[1];   // secondary color

/**
 * You can access R, G, B values of these colors with following syntax
 */
effects[activeEffectId].color[0].R; // Red (0-255) of primary color
```
```c++
/**
 * This are the uint32_t parameter.
 * Indices go from 0-4.
 * Note: para[0] is reserved for time (in ms) between to frames.
 *       So in general you shouldn't use this inside the effect function.
 *       (There are exceptions I will show you later)
 *
 * These variables are not protected against writing,
 * but NEVER write new values to these variables!
 * Just read them to a local variable which you can manipulate if you want.
 */
effects[activeEffectId].para[1];
```

---

For strip (pixel) manipulation there are following functions available:

```c++
/**
 * Set the color of one pixel with the index i.
 * You can supply a RgbColor object or individual RGB values.
 */
strip->SetPixelColor(i, color);             // with RgbColor object
strip->SetPixelColor(i, RgbColor(r,g,b));   // with individual RGB values
```
```c++
/**
 * Set the color of the whole strip.
 * if show is true strip->Show() will be called inside
 */
setStripColor(color, show);
```
```c++
/**
 * SetPixelColor() (or other functions) sets the new color only in an internal buffer.
 * To push this internal buffer to the real strip this has to be called.
 * You should only call this once (at the end) in your function.
 */
strip->Show();
```

---

With all these variables and functions you should be able to create (nearly) every effect you want.  

```c++
if(effectTasks[activeEffectId].isLastIteration()) {
    // set the iterations of your effect
    enableEffect(iterations);
}
```

Add this to the end of your function. Insert your needed number of iterations.  
There are two kind of effects, _dependent_ and _independent_ effects. 

- __dependent__  
  Every frame of your effect is based on the last one. Or at least it follows a time/frame based pattern.  
  General this means you are using `getRunCounter()`.  
  In this case your iterations are a number greater then 1.  
  For example in the rainbow effects its 128, because the rainbow pattern has 128 different frames until it repeats (start again from beginning).  
  Choose this number as small as possible. For a simple blink effect (on/off) it will be 2.  
  It has 2 steps until it repeats. Even if 4, 6, 8, ... would work also, but it's bad practise and will increase the time if the user has changed the effect.  
  Also the timesync of the nodes will be called less often (so in extreme cases your effect can drift from the common timebase).

- __independent__  
Every frame is completly idependent from the last one or any counter variable. An example for this is the fire effect.  
An independent effect has only 1 iteration.  
But there is an exception : In general between two frames there is a fixed delay. For some independent effects (like the fire effect) we want that delay to be variable. In this case pass 0 as iterations, but your effect has to set the next delay by itself with following lines.  
```c++
/**
 * This sets the interval (delay between two frames) of the effect manual
 */
effectTasks[activeEffectId].setInterval(nextDelay);

/**
 * For example the next delay of the fire effect is the sum of a
 * fixed delay and a random delay, which are defined in the effect parameter.
 */
uint16_t fixedDelay = effects[activeEffectId].para[0];
uint16_t randomDelay = ESP8266TrueRandom.random(effects[activeEffectId].para[1]);
uint16_t nextDelay = fixedDelay + randomDelay;
```

# defaults.h
In this file the default colors and parameters are defined.  
This happens with a Json string which is also used to save the user configures profile to the ESP filesystem - I will not go deeper into this topic here.  

But because the json string in the `defaults.h` file is pretty much unreadable, changes to the default settings will be made in the documentation and then copied into the `defaults.h` file.  

Please jump now to the [Documentation -> Configs.md](#configsmd) section. After this is finished come back here.

Now copy the complete Json object from [Configs.md](Configs.md)
```json
{
    "activeEffectId": {
        "version": [0,1,0],
        "value": 0
    },
    "effects": [
        [....] 
    ]
}
```
and paste it to this (or another) Json formater: https://jsonformatter.curiousconcept.com/  
Set the `Json Template` to `Compact` and press `Process`.

Now copy the formated Json string (it should now look as ugly as the one in `defaults.h`) and past it in the `effectsDefaultJson` object.  

__NOTE:__ Take care you past it in the proper position (inside the brackets): 
```c++
char *effectsDefaultJson = (char*)R"(formatedJsonStringHere)";
```

# whoopLights.ino
```c++
/*#######################################################
##                      Defines                        ##
#######################################################*/

const uint8_t version[] = {0,1,0};
```
Insert the new version from [Configs.md](#configsmd) also here.

# App Code

# effects.js
`src/store/reducer/effects.js`

```js
const initialState = {
    effects: [
        {
            name: "Rainbow",
            description: "A rainbow cycles around the gate. Speed and number of full rainbows around the gate can be changed.",
            para: [
                [....]
            ]
        },
        {
            name: "Fire",
            description: "This looks like a flickering fire - More or less.. ;) Color, flickering speed and intensity can be changed.",
            para: [
                [....]
            ]
        }
    ]
}
```

Add an entry to the end of the `effects` array with following pattern:
```js
{
    name: "Rainbow",                    // Name of your effect
    description: "A[....] changed.",    // Short description what it looks like
    para: [                             // Array of all parameter
        {
            name: "Speed",                          // Name of parameter
            description: "Delay [....] movement.",  // Description what it changes and eg. valid input range.
            enabled: true                           // Is this parameter used?
        },
        {
            name: "Number",
            description: "Number [....] strip.",
            enabled: true
        },
        {
            name: "Direction",
            description: "0 [....] reversed.",
            enabled: true
        },
        {
            name: "",           // For not used parameter leav name and description free
            description: "",
            enabled: false      // This parameter is not used
        },
        {
            name: "",
            description: "",
            enabled: false
        }
    ]
},
```



# Documentation

## Configs.md
Go to the [Version](Configs.md#version) section.  
The version has standard format:
```
major.minor.patch
```
General new effects will be added in a new minor release, so add one to minor and reset patch to 0.

Examples:
```
0.1.0 -> 0.2.0
1.2.6 -> 1.3.0
```
Please remember the new version, we will need it a few times.

---

Go to the [Effects](Configs.md#effects) section.  
Here is the Json effects configuration. Search for the `effects` key (it's an array).
```json 
"effects": [
    {
        "name": "Rainbow",
        "version": [0,1,0],
        "color": {
            "primary": [0,0,0],
            "secondary": [0,0,0]
        },
        "para": [14,2,0,0,0]
    },  
    {
        "name": "Fire",
        "version": [0,1,0],
        "color": {
            "primary": [255,96,12],
            "secondary": [0,0,0]
        },
        "para": [30,160,70,0,0]
    } 
]
```
Add an entry at the end of the existing array with following format:  
(I will add c++ style comments in this example - the are NOT allowed in Json)
```json
{
    "name": "Example",              // Your effect name as string
    "version": [0,2,0],             // The above increased version as integer array
    "color": {
        "primary": [255,0,0],       // Primary colors as integer array
        "secondary": [0,0,0]        // Secondary color as integer array
    },
    "para": [100,22,33,44,0]        // Your (up to) 5 parameter         
}, 
```

Not you can go back to [defaults.h](#defaultsh).

## Effects.md
Add your Effect with basic description, color and parameter description here.  
You can you descriptions from [effects.js](#effectsjs) here.

Also please add some good examples.  
_ToDo: Add section about gifs and parameter_