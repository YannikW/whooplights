# ToDo

Here are some ideas what things has to be done.  
There are ordered in high, medium or low priority.

Here are some examples what kind of problem / feature should be sorted where.
- High priority: Bugfixes
- Medium priority: Features
- Low priority: Effects

## High priority

## Medium priority

- Version control in the app
    - Add version to the status command
    - App reads this version end enable, disable features depending on it
        - List of effects could be included the version in which they are introduced
    - Inform the user if the version is outdated  
- Color description and enabled definition in app

## Low priority

- New mobile optimized color picker
- Color gamma correction, so led colors looks like in the color picker