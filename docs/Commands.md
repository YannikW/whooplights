# Table of Content

- [Basic Command Structure](#basic-command-structure)
- [Command List](#commandlist)
    - [Master -> Client](#master-client)
    - [Client -> Master](#client-master)

# Basic Command Structure

Commands are send in JSON format in following (example) pattern:

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "setEffect",
    "data": {
        "effectId": 2
    }
}
```

### type
The type key must be present. It defines what kind of message this is.  
Following types are supported currently:

| type | description |  
| :----: | --- |  
|  1   | Broadcast message. It will be executed by every node in the network. |  
|  2   | Targeted message. It will be executed only by the node which matches the id in the "target" key. |  

### source
The ID of the sender.

### target
If type is 2 (targeted message) the target is is defined here. For other types this key is optional.

### command
This is the command string. It must be present.  
See [Command List](#command-list) for all possible options.

### data
This is a container for all command specific payload.  
For details see the description for each command.

# Command List

- [Master -> Client](#master-client)
    - [getPara](#getpara)
    - [getSettings](#getsettings)
    - [getStatus](#getstatus)
    - [resetColor](#resetcolor)
    - [resetPara](#resetpara)
    - [resetSettings](#resetsettings)
    - [setAlias](#setalias)
    - [setColor](#setcolor)
    - [setEffect](#seeEffect)
    - [setPara](#setpara)
    - [setSettings](#setsettings)

- [Client -> Master](#client-master)
    - [para](#para)
    - [settings](#settings)
    - [status](#status)

## Master -> Client
(App -> Controller)

## getPara
This will trigger the node to send a [para](#status).  

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "getPara",
    "data": {
    }
}
```

## getSettings
This will trigger the node to send a [settings](#settings).  

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "getSettings",
    "data": {
    }
}
```

## getStatus
This will trigger the node to send a [status](#status).  

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "getStatus",
    "data": {
    }
}
```

## resetColor
Reset colors for one effect to default values.  
[para](#para) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "resetColor",
    "data": {
        "effectId": 1
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | This effect will be resetted. |


## resetPara
Reset parameter for one effect to default values.  
[para](#para) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "resetPara",
    "data": {
        "effectId": 1
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | This effect will be resetted. |

## resetSettings
Reset settings of this node to default.  
This includes everything listed in [Settings@Configs.md](Configs.md#settings).  
[settings](#settings) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "resetSettings",
    "data": {
    }
}
```

## setAlias
Change the alias of the node.  
Default is the node id.  
[status](#status) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "setAlias",
    "data": {
        "name": "Alfa"
    }
}
```

| Key | Description|
| --- | --- |
| `name` | New alias name for the node. |


## setColor
Set the effect colors.  
[para](#para) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "setPara",
    "data": {
        "effectId": 1,
        "color": {
            "primary": [255,0,0],
            "secondary": [0,255,0]
        }
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | ID of the currently enabled effect. |
| `color->primary` | RGB of the primary effect color. |
| `color->secondary` | RGB of the secondary effect color. |

## setEffect
Change the active effect.  
[status](#status) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "setEffect",
    "data": {
        "effectId": 2
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | Numeric id for the new effect.<br>See this [ToDo: List of effects]() for effect descriptions. |

## setPara
Set the effect parameters.  
[para](#para) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "setPara",
    "data": {
        "effectId": 1,
        "para": [1,2,3,4,5]
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | ID of the currently enabled effect. |
| `para` | Five effect parameter. |

## setSettings
Set the basic node settings.  
[settings](#settings) command will be send back if successfull.

```json
{
    "type": 2,
    "source": 12345678,
    "target": 87654321,
    "command": "setSettings",
    "data": {
        "numLeds": 20,
        "voltageScale":  17661,
        "warningCellVoltage": 3650,
        "brightness": 255
    }
}
```

| Key | Description|
| --- | --- |
| `numLeds` | Number of configured leds. |
| `voltageScale` | Internal scaling factor between analogRead and real voltage (in uV). |
| `warningCellVoltage` | Under this voltage (per cell) the gate will stop normal working mode. |
| `brightness` | User defined brightness (0-255). |

## Client -> Master
(Controller -> App)

## para
Response to [getPara](#getpara) command.  
It contains all parameters of current enabled effect

```json
{
    "type": 2,
    "source": 87654321,
    "target": 12345678,
    "command": "para",
    "data": {
        "effectId": 1,
        "color": {
            "primary": [255,0,0],
            "secondary": [0,255,0]
        },
        "para": [1,2,3,4,5]
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | ID of the currently enabled effect. |
| `color->primary` | RGB of the primary effect color. |
| `color->secondary` | RGB of the secondary effect color. |
| `para` | Five effect parameter. |

## settings
Response to [getSettings](#getsettings) command.  
It contains basic information about the node.  

```json
{
    "type": 2,
    "source": 87654321,
    "target": 12345678,
    "command": "settings",
    "data": {
        "numLeds": 20,
        "voltage": 12.34,
        "voltageScale":  17661,
        "warningCellVoltage": 3650,
        "brightness": 255
    }
}
```

| Key | Description|
| --- | --- |
| `numLeds` | Number of configured leds. |
| `voltage` | Input voltage. |
| `voltageScale` | Internal scaling factor between analogRead and real voltage (in uV). |
| `warningCellVoltage` | Under this voltage (per cell) the gate will stop normal working mode. |
| `brightness` | User defined brightness (0-255). |


## status
Response to [getStatus](#getstatus) command.  
It contains basic information about the node.  

```json
{
    "type": 2,
    "source": 87654321,
    "target": 12345678,
    "command": "status",
    "data": {
        "effectId": 1,
        "alias": "Alfa",
    }
}
```

| Key | Description|
| --- | --- |
| `effectId` | ID of the currently enabled effect. |
| `alias` | Alias name of the node for easier identification.<br>Default is the node id. |