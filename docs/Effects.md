# Effects

- [Rainbow](#rainbow)
- [Fire](#fire)
- [Tunnel](#tunnel)
- [Larson Scanner](#larsonscanner)
- [Snake](#snake)
- [Theater Chase](#theaterchase)
- [Strobe](#strobe)
- [Police](#police)

## Rainbow
A rainbow cycles around the gate. Speed and number of full rainbows around the gate can be changed.

### Colors
1. __Primary:__ _not used_
2. __Secondary:__ _not used_

### Parameter
1. __Speed:__ Delay in ms between two frames. Smaller values means faster movement.
2. __Number:__ Number of complete rainbows from beginning to end of the strip.
3. __Direction:__ 0 = normal, 1 = reversed.

### Examples
![giphy](https://media.giphy.com/media/Lp57UacUjZjFNlwxIe/giphy.gif)  
Left: (Para: [14, 4, 0])  
Right: (Para: [14, 2, 0])

## Fire
This looks like a flickering fire - More or less.. ;) Color, flickering speed and intensity can be changed.

### Colors
1. __Primary:__ Base color.
2. __Secondary:__ _not used_

### Parameter
1. __Speed fixed:__ Minimum Delay in ms between two frames. Smaller values means faster flickering.
2. __Speed random:__ Additional random (max) delay, will be added to fixed delay.
3. __Flicker:__ Greater values means heavier flickering.

### Examples
![giphy](https://media.giphy.com/media/9PrChVW7oLHt6voWB6/giphy.gif)  
Left: (Colors: [255, 96, 12] Para: [30, 160, 70])  
Right: (Colors: [41, 138, 31] Para: [30, 50, 255])

## Tunnel
This effect is used with multiple gates in a tunnel configuration. Set total number of gates and individual number in the tunnel in the parameter.

### Colors
1. __Primary:__ Color when gate is on.
2. __Secondary:__ Color when gate is off.

### Parameter
1. __Timestep:__ Delay until the next gate in the tunnel light up.
2. __Number of gates:__ Total number of gates.
3. __Position:__ Individual position (first, second, third, ...).
4. __Stay enabled mode:__ 0 = off, 1 = on. If enabled the gate stays on and don't go off until cycle repeats.

### Examples
![giphy](https://media.giphy.com/media/f9ShyKj22a21X1dLPS/giphy.gif)  
(Colors: [255, 0, 40], [0, 30, 30] Para: [200, 4, _X_, 1])  
![giphy](https://media.giphy.com/media/dAJlVCySusaYV7QkND/giphy.gif)  
(Colors: [255, 0, 40], [0, 30, 30] Para: [200, 4, _X_, 0])

## Larson Scanner
Classic larson scanner. Also known as KITT or Knightrider.

### Colors
1. __Primary:__ Color of the running light.
2. __Secondary:__ Background color.

### Parameter
1. __Speed:__ Delay in ms between two frames. Smaller values means faster movement.  
2. __Width:__ Width of the running light.

### Examples
![giphy](https://media.giphy.com/media/n5HJqHLTlC7p8r3pmR/giphy.gif)  
Left: (Colors: [255, 0, 0], [40, 40, 40] Para: [30, 5])  
Right: (Colors: [15, 212, 42], [200, 40, 97] Para: [30, 5])  

## Snake
Snake goes around the gate.

### Colors
1. __Primary:__ Head of the snake.
2. __Secondary:__ Tail of the snake.

### Parameter
1. __Speed:__ Delay in ms between two frames. Smaller values means faster movement.  

### Examples
![giphy](https://media.giphy.com/media/1svxiYJM0NNbit2knk/giphy.gif)  
Left: (Colors: [255, 70, 0], [0, 70, 70] Para: [50])  
Right: (Colors: [255, 0, 118], [36, 216, 97] Para: [25]) 

## Theater chase
Blinking funfair lights.

### Colors
1. __Primary:__ Color of the "On" pixels.
2. __Secondary:__ Color of the "Off" pixels.

### Parameter
1. __Speed:__ Delay in ms between two frames. Smaller values means faster movement.  
1. __On Leds:__ How many leds in an "On" block.
1. __Off Leds:__  How many leds in an "Off" block.

### Examples
![giphy](https://media.giphy.com/media/fHiwOzzXGMV9Kvx2mp/giphy.gif)  
Left: (Colors: [77, 0, 255], [200, 200, 0] Para: [50, 2, 3])  
Right: (Colors: [0, 0, 0], [200, 200, 0] Para: [50, 10, 10]) 

## Strobe
Strobe effect.

### Colors
1. __Primary:__ Strobe color.
2. __Secondary:__ _not used_

### Parameter
1. __Speed:__ Delay in ms between On/Off toggle. Smaller values means faster flashing.

### Examples
__Note__: Strobe effect looks much different in real - framerate of the gif can't catch up with the real strob frequency
![giphy](https://media.giphy.com/media/1d5WPhy3bEiNTktBFd/giphy.gif)  
Left: (Colors: [255, 255, 255], [0, 0, 0] Para: [25])  
Right: (Colors: [255, 255, 255], [0, 0, 0] Para: [250]) 

## Police
Flashing police light.

### Colors
1. __Primary:__ Color of first two flashes.
2. __Secondary:__ Color of third flash.

### Parameter
1. __Speed:__ Delay in ms between On/Off toggle. Smaller values means faster flashing.
2. __Pause multiplier:__ Number of pause frames (each with the 'Speed' delay) between side change.

### Examples
![giphy](https://media.giphy.com/media/bqYNFSP6s5Fg4jTtr3/giphy.gif)  
(Colors: [255, 0, 0], [0, 0, 255] Para: [50, 2])