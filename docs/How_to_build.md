# Table of Content

- [Gate](#gate)
- [Led Controller](#led-controller)
    - [Part List](#part-list)
    - [LED Strips](#led-strips)
    - [Order PCBs](#order-pcbs)
    - [PCB Soldering](#pcb-soldering)
    - [Flash Firmware](#flash-firmware)
- [App](#app)
    - [Installation](#installation)
    - [Usage](#usage)
        - [Connect page](#connect-page)
        - [Main page](#main-page)

# Gate
The gate is completly designed for 3D printing.  
You can find all files and informations on thingiverse: https://www.thingiverse.com/thing:3169911

# Led Controller

## Part List
Some links here are reflinks.  
If you like this project I would appreciate if you buy the parts through my reflinks.
For every order you make after clicking on my reflink I will get a small commission.  
__It is completly free for you!__ You don't have to pay more!

| Part | Quantity | Price | Link | Remark |
| --- | --- | --- | --- | --- |
| WS2811 led strip 60 leds/m | 1m (S), 1.25m (M), 1.5m (L) | 2,98$ - 4,35$ | [Aliexpress.com](https://www.aliexpress.com/item/5m-DC12V-Black-PCB-30-48-60-leds-m-10-16-20-ws2811-ic-meter-10/2000165819.html) | You should order a 5m roll, it's cheapest per meter. |
| LTC PCB | 1 | 0,20$ | [Elecrow.com](http://www.elecrow.com/referral-program/NDY2MWoydA==/) | One order will be 60-72 pcs. |
| DC-DC 5V Step Down | 1 | 1,02$ | [Banggood.com](https://www.banggood.com/10pcs-DC-DC-5V-3A-Power-Supply-Module-Buck-Step-Down-Regulator-Module-24V-12V-9V-To-5V-Fixed-Output-p-1213500.html?&p=A418101362674201503X) | (10pcs) Also available in other quantities. |
| NodeMCU ESP8266 | 1 | 4,19$ | [Banggood.com](https://www.banggood.com/NodeMcu-Lua-WIFI-Internet-Things-Development-Board-Based-ESP8266-CP2102-Wireless-Module-p-1097112.html?&p=A418101362674201503X) |
| SMD Resistor 100R 0805 | 1 | 0,01$ | [Reichelt.de](https://www.reichelt.de/smd-widerstand-0805-100-ohm-125-mw-1-smd-0805-100-p32874.html?) (germany) | |
|  |  | 0,089$ | [Digikey.com](https://www.digikey.com/product-detail/en/yageo/RT0805FRE07100RL/YAG3360CT-ND/5418065) (worldwide) | _alternative worldwide supplier_ |
| SMD Resistor 91k 0805 | 1 | 0,01$ | [Reichelt.de](https://www.reichelt.de/smd-widerstand-0805-91-kohm-125-mw-1-rnd-0805-1-91k-p183274.html?) (germany) | | 
|  |  | 0,046$ | [Digikey.com](https://www.digikey.com/product-detail/en/yageo/RC0805FR-0791KL/311-91.0KCRCT-ND/731149) (worldwide) | _alternative worldwide supplier_ |
| SMD Resistor 330k 0805 | 1 | 0,01$ | [Reichelt.de](https://www.reichelt.de/smd-widerstand-0805-330-kohm-125-mw-1-smd-0805-330k-p32916.html?) (germany) | |
|  |  | 0,097$ | [Digikey.com](https://www.digikey.com/product-detail/en/yageo/RT0805FRE07330KL/YAG5980CT-ND/9371010) (worldwide) | _alternative worldwide supplier_ |
| 74AHCT1G125GW | 1 | 0,07$ | [Reichelt.de](https://www.reichelt.de/single-dual-gate-logik-sot-353-5-74ahc-t1g125gw-p219156.html?) (germany) | |
|  |  | 0,235$ | [Digikey.com](https://www.digikey.com/product-detail/en/nexperia-usa-inc/74AHCT1G125GW125/1727-4055-1-ND/1965366) (worldwide) | _alternative worldwide supplier_ |
| _Total:_ |  | ~9$ | | depends on strip length and supplier |

## LED Strips
My recommendation are WS2811 strips. I'm sure you heard about WS2812 or WS2812b.  
Well - WS2811 ist mostly the same with a few differences:  
- 12V instead of 5V  
    - We don't need a big 5V Stepdown. With a software trick we can power it direct from 3S to 4S lipo.
- 3 Leds connected to one controller
    - On WS2812 you can controll every led individual, on WS2811 you can only control a block of 3 leds. (So 1m strip with 60 leds acts like 20 leds in the software). Not to understand me wrong: of cause this is a disadvantage ;D But for this application it's not an issue..
- __They are cheaper__
    - That's the main argument using WS2811 strips. Cheaper strips and no need for a big stepdown gives us a few dollars less per gate.. Let's build 10 gates and you will save a lot of money with no disadvantages in the real world.

## Order PCBs
You could build a controller without the pcb and just wire everything together. Thats fine but personal I like it clean and because this pcbs are so cheap you should order some, too.

Once you are on the [Elecrow Mainpage](http://www.elecrow.com/referral-program/NDY2MWoydA==/) navigate to the PCB services section.  

![_Elecrow mainpage navigation_](pics/elecrow_main_page.PNG)

Now select the same options as in the picture:

![_Elecrow pcb services_](pics/elecrow_pcb_services.PNG)

1. Upload the panelized gerber zip file from the latest release [here](https://gitlab.com/YannikW/whooplights/tags)
2. Set size to 80 x 82mm
3. Set quantitiy to 10 (same price as 5pcs...)
4. Select your country and shipping methode

Optional you can select different pcb thickness and color as you like.  
(Thinner pcbs are lighter and normaly elecrow will calculate less shipping. I recommend 1mm minimum.)

Add to your cart and order - that's it.  
Manufacturing normaly takes about one week.  
From this order you will get 60-72 pcbs, so you can share it with your friends :)  
Order less pcs isn't worth it. You will get half the pcbs and maybe save 1$ or something like this..  
You will get 10-12 panels with 6 pcbs each. They are v-cutted, so you can break them into individual pcbs easy.

_ToDo: Add picture of a pcb panel_

## PCB Soldering

_ToDo: Detailed instructions_

Solder everything on the pcb. Care for the orientation of NodeMCU and stepdown.

R1: 330k  
R2: 91k  
R3: 100R  

U3: 74AHCT1G125GW (this is hard so solder.. You need experience and tools for smd soldering to do this right!)

6 Pin header is for the led strips. Three pins per strip.  
They are internal connected - you cant connect two differnt strips. It's the same signals on both channels.

__G:__ Ground  
__S:__ Signal  
__+:__ Power (Care! Input voltage here - only for WS2811)

For WS2812 connect ground and signal to the pads, but voltage has to be connected to a 5V source. You can solder to the positiv output pad of the stepdown (right bottom pad, labeled with "+" on the black stepdown board). But take care of the power consumption and heat on the stepdown.  

__Note:__ WS2812 not fully supported for now. Led strip will be dimmed if input voltage is above 12V (needed for WS2811). This can't be turned of right now. No problem if you using 3S LiPo or 12V power supply.

Finished PCB should look like this:

(Ignore the old name on the pcb, _whoopLights_ sound much cooler, don't it?)
![_pcb V0.1_](pics/pcb_01.jpg)

## Flash Firmware
Flashing the firmware is very easy ;)  
You don't need to install any software - everything you need comes in the release firmware zip package (see releases [here](https://gitlab.com/YannikW/whooplights/tags)). Awesome, right?  

First plug the ESP8266 into your computer and make sure it's powered on.  

Navigate to the `Flashtool` folder.  
Just doubleclick the `FLASH.bat` file and a windows command promp should open.  

It should look more or less like this: 
```
whoopLights Flashtool V0.1


Available COM ports:
_COM1=Kommunikationsanschluss
_COM3=Silicon Labs CP210x USB to UART Bridge


Please enter the ESP8266 COM port number (search for CP210x in above list)
```

Of cause on your machine other available COM ports will be listed.  
If you bought the NodeMCU I've lised in the [partlist](#part-list), you should see one device with this description:
```
Silicon Labs CP210x USB to UART Bridge
``` 

Now just enter the COM port number of this device (in this example you could type `3` or `COM3`) and hit enter.

If everything is okay the output should look like this:
```
Connecting . connection established
Erasing 390672 bytes...
Writing block 382 of 382 at 0x05f400
390672 bytes written successfully.
```

If something went wrong (probably wrong COM port) it looks like this:
```
Connecting ................ connection attempt failed
```
In this case make sure your ESP8266 is plugged in and powered on correctly. If this is the case try another COM port or check for correct drivers.  

# App
## Installation
For now there is only an Android application available.

whoopLights app is currently __not__ available in the Google PlayStore - you have to install it manually.

Download the lastest _.apk_ file from [here](https://gitlab.com/YannikW/whooplights/tags) to your phone and open it to install.

If android deny the installation make sure you have enabled _Unknown Sources_ to install apps from other sources then the PlayStore.  
You can find this option under _Settings > Security > Unknown Sources_ (something like this - it depends on you android version and manufacturer).


## Usage
One word in advance: If you notice any issue, please read the [Troubleshooting](../README.md#troubleshooting) section.  
If the issue still exist open an issue here at GitLab - I will try to help asap.

Using the whoopLights app is pretty straight forward.

Open the app and, wait a few seconds for connection and start configuring your gates.

In 99% the app will connect to your controllers automatically - just wait a few seconds (normally it shouldn't take longer then 20 seconds to connect). Refer to the [Troubleshooting](../README.md#troubleshooting) section if it doesn't connect. 

### Connect page
The connect page is the one you enter when opening the app. 

![app_connect_screen](pics/app_connect_screen.jpg)

Yah.. Pretty much information here, right? :P

Software tries to establish a connection to the _whoopLights_ wifi network (SSID: "whoopLights", Password: "12345678") and then connect to the controller.  
Everyhthing happens automatic here and when connection was successfull you will be forwarded to the [Main page](#main-page).

### Main page

![app_main_screen](pics/app_main_screen.jpg)

This examples shows thhe app with two controller connected. First controller is opened in the detailed view.
The list is sorted alphabetical by the alias.

__#1: Alias__  
This is the alias / name of the controller. Default is the internal id. You should change it to something more usefull.

__#2: Enabled Effect Name__  
The name of the current enabled effect.

__#3: Effect Selection__  
Here you can switch through the effect. You always get the name and a short decription of the effect.

__#4: Alias button__  
Tap to change the alias of the controller. It will be saved permanent on the controller. You can choose whatever you want.

__#5: Colors button__  
Tap to change the primary and secondary effect of the current enabled effect.  
_Note: Not all effects use these colors. In future this will only be enabled if the colors can be changed._

__#6: Settings button__  
Tap to open the controller settings. For example here you can define how much leds your strip has or the brightness of your strip.

__#7: Parameter button__  
Tap to see and change the parameter of the current enabled effect. Every effect has different parameter. Some even has none.

__#8: Other controllers__  
Every controller in the network is shown through one line with the alias and the current enabled effect (see #1 and #2). Tapping on another controller will close the detailed view of the current selected and open the detailed view of the new one.

__#9: Update nodes__  
Tap to refresh the list of controllers (also called nodes).