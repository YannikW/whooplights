/*
 * File:			CmdHandler.js
 * Project:			ltc-app
 * Created:			19.09.2018, 20:12:22
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { myId, TYPE_BROADCAST, TYPE_TARGET } from '../Sockette/Sockette';
import * as actions from '../../store/actions/index';

export const handleCmd = (jsonObj, dispatch) => {
    // Is is either a broadcast or targeted to me
    if(jsonObj.type === TYPE_BROADCAST || (jsonObj.type === TYPE_TARGET && jsonObj.target === myId)) {
        switch(jsonObj.command) {
            case "status":
                dispatch(actions.cmdStatus(jsonObj.source, jsonObj.data));
                break;
            
            case "para":
                dispatch(actions.cmdPara(jsonObj.source, jsonObj.data));
                break;

            case "settings":
                dispatch(actions.cmdSettings(jsonObj.source, jsonObj.data));
                break;
            
            default:
                console.log("handleCmd(): No matching command found");              
        }
    }
}