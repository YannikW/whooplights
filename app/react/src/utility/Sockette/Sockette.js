/*
 * File:			Sockette.js
 * Project:			ltc-app
 * Created:			19.09.2018, 14:35:31
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:11:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import Sockette from 'sockette';

let ws = null;

export const myId = 123;
export const TYPE_TARGET = 2;
export const TYPE_BROADCAST = 1;

export const createWs = (ip, onOpen, onMessage, onReconnect, onMaximum, onClose, onError) => {
    //const wsURL = 'ws://' + ip[0] + '.' + ip[1] + '.' + ip[2] + '.' + ip[3] + '/ws';
    const wsURL = 'ws://www.whooplights.esp/ws';

    if (ws) {
        ws.close();
    }
    ws = null;
    ws = new Sockette(wsURL, {
        timeout: 500,
        maxAttempts: -1,
        onopen: e => onOpen(e),
        onmessage: e => onMessage(e),
        onreconnect: e => onReconnect(e),
        onmaximum: e => onMaximum(e),
        onclose: e => onClose(e),
        onerror: e => onError(e)
      });
}

export const closeWs = () => {
    if (ws) ws.close();
}


export const sendCmd = (target, cmd, data, connected) => {
    if (ws && connected) {
        const jsonPackage = {
            type: TYPE_TARGET,
            source: myId,
            target: target,
            command: cmd,
            data: data
        };

        console.log('Cmd send: ', JSON.stringify(jsonPackage));
        ws.send(JSON.stringify(jsonPackage));
    }
}

export const sendBroadcast = (cmd, data, connected) => {
    if (ws && connected) {
        const jsonPackage = {
            type: TYPE_BROADCAST,
            source: myId,
            command: cmd,
            data: data
        };

        console.log('Cmd send: ', JSON.stringify(jsonPackage));
        ws.send(JSON.stringify(jsonPackage));
    }
}


export default ws;