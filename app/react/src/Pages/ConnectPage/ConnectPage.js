/*
 * File:			ConnectPage.js
 * Project:			ltc-app
 * Created:			19.09.2018, 14:08:27
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:38:44
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { CircularProgress } from '@material-ui/core';

const styles = theme => ({
    gridContainer: {
        display: 'grid',
        gridTemplateRows: 'auto 1fr auto',
        gridTemplateColumns: 'auto auto',
        gridTemplateAreas: `
            'made made'
            'cprogressBar cprogressBar'`,
        alignItems: 'center',
        justifyItems: 'center',
        height: 'calc(100vh - 16px)'
    },
    subGridContainer: {
        display: 'grid',
        gridTemplateRows: 'auto auto auto auto',
        gridArea: 'cprogressBar',
        alignItems: 'center',
        justifyItems: 'center',
    },
    cprogressBar: {
        margin: '32px',
    },
    ipField: {
        gridArea: 'ipField',
        alignSelf: 'end',
        justifySelf: 'end'
    },
    manualButton: {
        gridArea: 'manualButton',
        alignSelf: 'end',
        justifySelf: 'start'
    },
    button: {
        margin: '8px'
    },
    ipLabel: {
        margin: '4px'
    },
    made: {
        gridArea: 'made',
        marginTop: '8px'
    }
});

class ConnectPage extends Component {
    state = {
        fetchOpen: false,
        
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.gridContainer}>
                <div className={classes.made}>
                    <Typography variant='caption'>
                        {"Made with " + String.fromCharCode(0x2764) + " by YannikW"} 
                    </Typography>
                </div>
                <div className={classes.subGridContainer}>
                    <div className={classes.cprogressBar}>
                        <CircularProgress
                            size={140}
                            thickness={5.0} />
                    </div>
                    <Typography variant='title'>
                        Connecting ...
                    </Typography>
                </div>     
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        websocket: state.websocket
    }
}

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ConnectPage));