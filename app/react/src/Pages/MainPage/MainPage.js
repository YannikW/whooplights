/*
 * File:			MainPage.js
 * Project:			ltc-app
 * Created:			19.09.2018, 15:11:28
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:37:42
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import NodeList from '../../Components/NodeList/NodeList';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    gridContainer: {
        display: 'grid',
        gridTemplateRows: 'auto 1fr auto',
        gridTemplateAreas: `
            'made'
            'nodesArea'
            'updateButton'`,
        alignItems: 'center',
        justifyItems: 'center',
        height: 'calc(100vh - 16px)',
    },
    nodesArea: {
        gridArea: 'nodesArea',
        alignSelf: 'start',
        justifySelf: 'start',
    },
    updateButton: {
        gridArea: 'updateButton',
        alignSelf: 'center',
        justifySelf: 'center',
    },
    button: {
        margin: '8px',
        minWidth: 'calc(100vw - 32px)'
    },
    made: {
        gridArea: 'made',
        margin: '8px'
    }
});

class MainPage extends Component {
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.gridContainer}>
                <div className={classes.made}>
                    <Typography variant='caption'>
                        {"Made with " + String.fromCharCode(0x2764) + " by YannikW"} 
                    </Typography>
                </div>
                <div className={classes.nodesArea}>
                    <NodeList/>
                </div>
                <div className={classes.updateButton}>
                    <Button 
                        variant="outlined" 
                        className={classes.button}
                        onClick={() => this.props.updateNodes(this.props.websocket.connected)} >
                        Update Nodes
                    </Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        websocket: state.websocket
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateNodes: (connected) => dispatch(actions.updateNodes(connected))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainPage));