/*
 * File:			  App.js
 * Project:			ltc-app
 * Created:			19.09.2018, 12:39:04
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:29:55
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import ConnectPage from './Pages/ConnectPage/ConnectPage';
import MainPage from './Pages/MainPage/MainPage';
import * as actions from './store/actions/index';

class App extends Component {
    render() {
        //Check if connection has dropped, fetch new IP and connect after new IP has been received
        if (this.props.websocket.connectionFailed) {
            this.props.connectWs(this.props.websocket.address);
        }

        let routes = (
            <Switch>   
                <Route path="/main" component={MainPage} />
                <Route path="/connect" component={ConnectPage} />
                <Redirect to="/connect" />
            </Switch>
        );

        return (
            <div>
                {routes}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        websocket: state.websocket
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
        fetchAutoIpAndConnect: () => dispatch(actions.fetchAutoIpAndConnect()),
        connectWs: (ip) => dispatch(actions.connectWs(ip)),
    };
  };
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
  