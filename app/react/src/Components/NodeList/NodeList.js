/*
 * File:			NodeList.js
 * Project:			ltc-app
 * Created:			20.09.2018, 10:28:10
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EffectDetail from '../EffectDetail/EffectDetail';

const styles = theme => ({
    root: {
        width: 'calc(100vw - 16px)',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '50%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
});

class NodeList extends Component {
    state = {
        expanded: null,
    };

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { classes } = this.props;
        const { expanded } = this.state;

        let nodesView = this.props.nodes.map((no, idx) => (
            <ExpansionPanel expanded={expanded === ('panel' + no.id)} onChange={this.handleChange('panel' + no.id)} key={no.id}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>{no.alias}</Typography>
                    <Typography className={classes.secondaryHeading}>{this.props.effects[no.effectId].name}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <EffectDetail nodeIndex={idx}/>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        ));

        return (
            <div className={classes.root}>
                {nodesView}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        nodes: state.nodes.nodes,
        effects: state.effects.effects
    }
}

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NodeList));