/*
 * File:			EffectDetail.js
 * Project:			ltc-app
 * Created:			20.09.2018, 11:07:59
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	07.10.2018, 11:51:18
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Button from '@material-ui/core/Button';
import { sendCmd } from '../../utility/Sockette/Sockette';
import TextDialog from '../TextDialog/TextDialog';
import DuoColorPicker from '../DuoColorPicker/DuoColorPicker';
import * as actions from '../../store/actions/index';
import ParaDialoge from '../ParaDialoge/ParaDialoge';
import SettingsDialoge from '../SettingsDialoge/SettingsDialoge';


const styles = theme => ({
    root: {
        display: 'grid',
        gridTemplateColumns: 'auto 1fr 1fr auto',
        gridTemplateAreas: `
            'effectDown effect effect effectUp'
            'effectDesc effectDesc effectDesc effectDesc'
            'information information information information'
            'buttonsLeft buttonsLeft buttonsRight buttonsRight'`,
        alignItems: 'center',
        justifyItems: 'center',
        width: '100%'
    },
    effectDown: {
        gridArea: 'effectDown'
    },
    effectUp: {
        gridArea: 'effectUp'
    },
    effect: {
        gridArea: 'effect'
    },
    effectDesc: {
        gridArea: 'effectDesc',
        margin: '8px 0'
    },
    information: {
        gridArea: 'information'
    },
    buttonsLeft: {
        gridArea: 'buttonsLeft',
        width: '100%',
        paddingRight: '8px',
        alignSelf: 'start'
    },
    buttonsRight: {
        gridArea: 'buttonsRight',
        width: '100%',
        paddingLeft: '8px',
        alignSelf: 'start'
    },
    updownbutton: {
        width: '38px',
        minWidth: '38px',
    },
    settingsButton: {
        marginTop: '8px'
    }
});

class EffectDetail extends Component {
    constructor(props) {
        super(props);
        this.colorDialogeCallbacks = undefined;
    }

    state = {
        aliasOpen: false,
        colorOpen: false,
        paraOpen: false,
        lastParaFetched: false,
        settingsOpen: false,
        lastSettingsFetched: false,
    };

    changeEffect = (inc, nodeId) => {
        const { effects } = this.props;
        const { nodes } = this.props;

        const currentEffectId = nodes.find(no => (no.id === nodeId)).effectId;

        let newEffectId;

        if (currentEffectId !== undefined) {
            if (inc) {
                // test for end of effect array
                if (currentEffectId === (effects.length - 1)) {
                    newEffectId = 0;
                }
                else {
                    newEffectId = currentEffectId + 1;
                }
            }
            else {
                // test for begin of effect array
                if (currentEffectId === 0) {
                    newEffectId = effects.length - 1;
                }
                else {
                    newEffectId = currentEffectId - 1;
                }
            }
            sendCmd(nodeId, "setEffect", { effectId: newEffectId }, this.props.websocket.connected);
        }
    }

    setAlias = () => {
        this.setState({
            aliasOpen: true
        });
    }

    aliasOnCloseHandler = () => {
        this.setState({
            aliasOpen: false
        });
    }

    aliasOnOkHandler = (newAlias) => {
        this.setState({
            aliasOpen: false
        });
        const activeNode = this.props.nodes[this.props.nodeIndex];
        sendCmd(activeNode.id, "setAlias", { name: newAlias }, this.props.websocket.connected);
    }

    setColor = () => {
        this.setState({
            colorOpen: true
        });
        // Fetch Parameter
        const activeNode = this.props.nodes[this.props.nodeIndex];
        this.props.fetchParameter(activeNode.id, this.props.websocket.connected);


    }

    pushColors = () => {
        // push colors to picker
        const activeNode = this.props.nodes[this.props.nodeIndex];
        if (this.colorDialogeCallbacks !== undefined) {
            const color = activeNode.parameter.color;
            this.colorDialogeCallbacks.setColor({
                primary: color.primary,
                secondary: color.secondary
            });
        }
    }

    colorOnCloseHandler = () => {
        this.setState({
            colorOpen: false
        });
    }

    colorOnOkHandler = (close, primaryColor, secondaryColor) => {
        if (close) {
            this.setState({
                colorOpen: false
            });
        }
        const activeNode = this.props.nodes[this.props.nodeIndex];

        const data = {
            effectId: activeNode.effectId,
            color: {
                primary: primaryColor,
                secondary: secondaryColor
            }
        }
        sendCmd(activeNode.id, "setColor", data, this.props.websocket.connected);
        this.props.clearParameterFetched(activeNode.id);
    }

    colorDialogeMounted = (callbacks) => {
        this.colorDialogeCallbacks = callbacks;
        //console.log('colorDialogeMounted()', this.colorDialogeCallbacks);
    }

    setPara= () => {
        this.setState({
            paraOpen: true
        });
        // Fetch Parameter
        const activeNode = this.props.nodes[this.props.nodeIndex];
        this.props.fetchParameter(activeNode.id, this.props.websocket.connected);
    }

    pushPara = () => {
        // push para to dialoge
        const activeNode = this.props.nodes[this.props.nodeIndex];
        if (this.paraDialogeCallbacks !== undefined) {
            const para = activeNode.parameter.para;
            this.paraDialogeCallbacks.setPara([...para]);
        }
    }

    paraOnCloseHandler = () => {
        this.setState({
            paraOpen: false
        });
    }

    paraOnOkHandler = (close, para) => {
        if (close) {
            this.setState({
                paraOpen: false
            });
        }
        const activeNode = this.props.nodes[this.props.nodeIndex];

        const data = {
            effectId: activeNode.effectId,
            para: [...para]
        }
        sendCmd(activeNode.id, "setPara", data, this.props.websocket.connected);
        this.props.clearParameterFetched(activeNode.id);
    }

    paraDialogeMounted = (callbacks) => {
        this.paraDialogeCallbacks = callbacks;
    }

    paraOnResetHandler = () => {
        const activeNode = this.props.nodes[this.props.nodeIndex];
        sendCmd(activeNode.id, "resetPara", {effectId: activeNode.effectId}, this.props.websocket.connected);
        this.props.clearParameterFetched(activeNode.id);
    }

    colorOnResetHandler = () => {
        const activeNode = this.props.nodes[this.props.nodeIndex];
        sendCmd(activeNode.id, "resetColor", {effectId: activeNode.effectId}, this.props.websocket.connected);
        this.props.clearParameterFetched(activeNode.id);
    }

    setSettings= () => {
        this.setState({
            settingsOpen: true
        });
        // Fetch Settings
        const activeNode = this.props.nodes[this.props.nodeIndex];
        this.props.fetchSettings(activeNode.id, this.props.websocket.connected);
    }

    pushSettings = () => {
        // push para to dialoge
        const activeNode = this.props.nodes[this.props.nodeIndex];
        if (this.settingsDialogeCallbacks !== undefined) {
            const settings = activeNode.settings;
            this.settingsDialogeCallbacks.setSettings({...settings});
        }
    }

    settingsOnCloseHandler = () => {
        this.setState({
            settingsOpen: false
        });
    }

    settingsOnOkHandler = (close, settings) => {
        if (close) {
            this.setState({
                settingsOpen: false
            });
        }
        const activeNode = this.props.nodes[this.props.nodeIndex];

        const data = {
            numLeds: settings.numLeds,
            voltageScale: settings.voltageScale,
            warningCellVoltage: settings.warningCellVoltage,
            brightness: settings.brightness
        }
        sendCmd(activeNode.id, "setSettings", data, this.props.websocket.connected);
        this.props.clearSettingsFetched(activeNode.id);
    }

    settingsDialogeMounted = (callbacks) => {
        this.settingsDialogeCallbacks = callbacks;
    }

    settingsOnResetHandler = () => {
        const activeNode = this.props.nodes[this.props.nodeIndex];
        sendCmd(activeNode.id, "resetSettings", {}, this.props.websocket.connected);
        this.props.clearSettingsFetched(activeNode.id);
    }

    render() {
        const { classes } = this.props;
        const { effects } = this.props;
        const { nodes } = this.props;
        const { settings } = this.props;
        const activeEffect = effects[nodes[this.props.nodeIndex].effectId];
        const activeNode = nodes[this.props.nodeIndex];

        const aliasDialoge = (
            <TextDialog
                title="Alias"
                label="Alias"
                text="Please enter a new alias."
                open={this.state.aliasOpen}
                default={activeNode.alias}
                onOk={(newAlias) => this.aliasOnOkHandler(newAlias)}
                onClose={this.aliasOnCloseHandler} />
        );

        const colorDialoge = (
            <DuoColorPicker
                title="Colors"
                open={this.state.colorOpen && activeNode.parameterFetched}
                onOk={(pC, sC) => this.colorOnOkHandler(true, pC, sC)}
                onApply={(pC, sC) => this.colorOnOkHandler(false, pC, sC)}
                onClose={this.colorOnCloseHandler}
                onMounted={this.colorDialogeMounted.bind(this)}
                onResetColor={this.colorOnResetHandler} />
        );

        const paraDialoge = (
            <ParaDialoge
                title="Parameter"
                open={this.state.paraOpen && activeNode.parameterFetched}
                onOk={(para) => this.paraOnOkHandler(true, para)}
                onApply={(para) => this.paraOnOkHandler(false, para)}
                onClose={this.paraOnCloseHandler}
                onMounted={this.paraDialogeMounted.bind(this)}
                effect={activeEffect}
                onResetPara={this.paraOnResetHandler} />
        );

        const settingsDialoge = (
            <SettingsDialoge
                title="Settings"
                open={this.state.settingsOpen && activeNode.settingsFetched}
                onOk={(settings) => this.settingsOnOkHandler(true, settings)}
                onApply={(settings) => this.settingsOnOkHandler(false, settings)}
                onClose={this.settingsOnCloseHandler}
                onMounted={this.settingsDialogeMounted.bind(this)}
                settings={settings}
                onResetSettings={this.settingsOnResetHandler} />
        );

        if(activeNode.parameterFetched && !this.state.lastParaFetched) {
            this.setState({
                lastParaFetched: true
            });

            this.pushColors();
            this.pushPara();
        }
        else if(!activeNode.parameterFetched && this.state.lastParaFetched) {
            this.setState({
                lastParaFetched: false
            });
        }

        if(activeNode.settingsFetched && !this.state.lastSettingsFetched) {
            this.setState({
                lastSettingsFetched: true
            });

            this.pushSettings();
        }
        else if(!activeNode.settingsFetched && this.state.lastSettingsFetched) {
            this.setState({
                lastSettingsFetched: false
            });
        }

        return (
            <div className={classes.root}>
                <div className={classes.effectDown}>
                    <Button
                        variant="outlined"
                        className={classes.updownbutton}
                        onClick={() => this.changeEffect(false, activeNode.id)} >
                        <KeyboardArrowLeftIcon />
                    </Button>
                </div>

                <div className={classes.effect}>
                    <Typography variant='body2'>
                        {activeEffect.name}
                    </Typography>
                </div>

                <div className={classes.effectUp}>
                    <Button
                        variant="outlined"
                        className={classes.updownbutton}
                        onClick={() => this.changeEffect(true, activeNode.id)} >
                        <KeyboardArrowRightIcon />
                    </Button>
                </div>

                <div className={classes.effectDesc}>
                    <Typography variant="caption">
                        {activeEffect.description}
                    </Typography>
                </div>

                <div className={classes.buttonsLeft}>
                    <Button
                        variant="outlined"
                        fullWidth={true}
                        className={classes.settingsButton}
                        onClick={this.setAlias} >
                        alias
                    </Button>
                    <Button
                        variant="outlined"
                        fullWidth={true}
                        className={classes.settingsButton}
                        onClick={this.setSettings} >
                        settings
                    </Button>
                </div>

                <div className={classes.buttonsRight}>
                    <Button
                        variant="outlined"
                        fullWidth={true}
                        className={classes.settingsButton}
                        onClick={this.setColor} >
                        colors
                    </Button>
                    <Button
                        variant="outlined"
                        fullWidth={true}
                        className={classes.settingsButton}
                        onClick={this.setPara} >
                        parameter
                    </Button>
                </div>

                {aliasDialoge}
                {colorDialoge}
                {paraDialoge}
                {settingsDialoge}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        nodes: state.nodes.nodes,
        settings: state.nodes.settings,
        effects: state.effects.effects,
        websocket: state.websocket
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchParameter: (id, connected) => dispatch(actions.fetchParameter(id, connected)),
        clearParameterFetched: (id) => dispatch(actions.clearParameterFetched(id)),
        fetchSettings: (id, connected) => dispatch(actions.fetchSettings(id, connected)),
        clearSettingsFetched: (id) => dispatch(actions.clearSettingsFetched(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(EffectDetail));