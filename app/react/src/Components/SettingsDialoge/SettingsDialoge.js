/*
 * File:			SettingsDialoge.js
 * Project:			ltc-app
 * Created:			23.09.2018, 12:21:30
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	07.10.2018, 11:38:59
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const styles = theme => ({
    gridContainer: {
        display: 'grid',
        gridTemplateColumns: '1fr',
    },
    button: {
        marginTop: '8px'
    }
});

class SettingsDialog extends Component {
    state = {
        settings: {
            numLeds: 0,
            voltage: 0,
            voltageScale: 0,
            warningCellVoltage: 0,
            brightness: 0
        }
    };

    componentDidMount() {
        if (this.props.onMounted) {
            this.props.onMounted({
                setSettings: settings => this.setSettings(settings)
            })
        }
    }

    handleChange = name => event => {
        let newSettings = { ...this.state.settings };
        newSettings[name] = event.target.value;
        this.setState({
            settings: newSettings,
        });
    };

    onClose = () => {
        this.props.onClose();
    }

    onOk = () => {
        this.props.onOk({ ...this.state.settings });
    }

    onApply = () => {
        this.props.onApply({ ...this.state.settings });
    }

    setSettings = (settings) => {
        this.setState({
            settings: { ...settings }
        });
    }

    resetSettings = () => {
        this.props.onResetSettings();
    }

    render() {
        const { classes, settings } = this.props;

        const numLedsDescription = settings.numLeds.description.map((el, idx) => (
            <Typography variant="caption" key={idx}>
                {el}
            </Typography>
        ));
        const voltageScaleDescription = settings.voltageScale.description.map((el, idx) => (
            <Typography variant="caption" key={idx}>
                {el}
            </Typography>
        ));
        const warningCellVoltageDescription = settings.warningCellVoltage.description.map((el, idx) => (
            <Typography variant="caption" key={idx}>
                {el}
            </Typography>
        ));
        const brightnessDescription = settings.brightness.description.map((el, idx) => (
            <Typography variant="caption" key={idx}>
                {el}
            </Typography>
        ));

        const textFields = (
            <div className={classes.gridContainer}>
                <TextField
                    label={settings.numLeds.name}
                    value={this.state.settings.numLeds}
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange("numLeds")}
                />
                {numLedsDescription}
                <TextField
                    label={settings.voltageScale.name}
                    value={this.state.settings.voltageScale}
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange("voltageScale")}
                />
                <Typography variant="body1">
                    {"Input voltage: " + this.state.settings.voltage + "V"}
                </Typography>
                {voltageScaleDescription}
                <TextField
                    label={settings.warningCellVoltage.name}
                    value={this.state.settings.warningCellVoltage}
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange("warningCellVoltage")}
                />
                {warningCellVoltageDescription}
                <TextField
                    label={settings.brightness.name}
                    value={this.state.settings.brightness}
                    margin="normal"
                    fullWidth
                    onChange={this.handleChange("brightness")}
                />
                {brightnessDescription}
            </div>
        );

        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.onClose}
                    aria-labelledby="form-dialog-title"
                    fullScreen
                >
                    <DialogTitle id="form-dialog-title">{this.props.title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {this.props.text}
                        </DialogContentText>
                        <div className={classes.gridContainer}>
                            {textFields}
                            <Button
                                variant="outlined"
                                fullWidth
                                className={classes.button}
                                onClick={this.resetSettings}
                            >
                                Reset to default
                            </Button>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.onOk} color="primary">
                            Save
                        </Button>
                        <Button onClick={this.onApply} color="primary">
                            Apply
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(SettingsDialog);