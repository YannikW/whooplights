/*
 * File:			ParaDialoge.js
 * Project:			ltc-app
 * Created:			21.09.2018, 10:18:30
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    gridContainer: {
        display: 'grid',
        gridTemplateColumns: '1fr',
    },
    button: {
        marginTop: '8px'
    }
});

class ParaDialog extends Component {
    state = {
        para: [0, 0, 0, 0, 0]
    };

    componentDidMount() {
        if (this.props.onMounted) {
            this.props.onMounted({
                setPara: para => this.setPara(para)
            })
        }
    }

    handleChange = idx => event => {
        let newPara = [...this.state.para];
        newPara[idx] = event.target.value;
        this.setState({
            para: newPara,
        });
    };

    onClose = () => {
        this.props.onClose();
    }

    onOk = () => {
        this.props.onOk([...this.state.para]);
    }

    onApply = () => {
        this.props.onApply([...this.state.para]);
    }

    setPara = (para) => {
        this.setState({
            para: [...para]
        });
    }

    resetPara = () => {
        this.props.onResetPara();
    }

    render() {
        const { classes, effect } = this.props;

        const textFields = effect.para.map((pa, idx) => {
            if (pa.enabled) {
                return (
                    <div key={idx}>
                        <TextField
                            label={pa.name}
                            value={this.state.para[idx]}
                            helperText={pa.description}
                            margin="normal"
                            fullWidth
                            onChange={this.handleChange(idx)}
                        />
                    </div>
                );
            }
            else {
                return null;
            }
        });

        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.onClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">{effect.name + " " + this.props.title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {this.props.text}
                        </DialogContentText>
                        <div className={classes.gridContainer}>
                            {textFields}
                            <Button
                                variant="outlined"
                                fullWidth
                                className={classes.button}
                                onClick={this.resetPara} 
                            >
                                Reset to default
                            </Button>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.onOk} color="primary">
                            Save
                        </Button>
                        <Button onClick={this.onApply} color="primary">
                            Apply
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(ParaDialog);