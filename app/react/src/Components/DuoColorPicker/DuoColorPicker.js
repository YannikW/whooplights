/*
 * File:			DuoColorPicker.js
 * Project:			ltc-app
 * Created:			20.09.2018, 14:52:12
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { SketchPicker } from 'react-color';

const styles = theme => ({
    dialogContent: {
        padding: '0 8px 24px'
    },
    button: {
        marginTop: '8px'
    }
});

class DuoColorPicker extends Component {
    state = {
        primaryColor: {
            r: 0,
            g: 0,
            b: 0
        },
        secondaryColor: {
            r: 0,
            g: 0,
            b: 0
        },
        value: 0
    };

    componentDidMount () {
        if (this.props.onMounted) {
            this.props.onMounted({
                setColor: color => this.setColor(color)
            })
        }
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };

    onClose = () => {
        this.props.onClose();
    }

    onOk = () => {
        const pcolor = [
            this.state.primaryColor.r,
            this.state.primaryColor.g,
            this.state.primaryColor.b
        ];
        const scolor = [
            this.state.secondaryColor.r,
            this.state.secondaryColor.g,
            this.state.secondaryColor.b
        ];
        this.props.onOk(pcolor, scolor);
    }

    onApply = () => {
        const pcolor = [
            this.state.primaryColor.r,
            this.state.primaryColor.g,
            this.state.primaryColor.b
        ];
        const scolor = [
            this.state.secondaryColor.r,
            this.state.secondaryColor.g,
            this.state.secondaryColor.b
        ];
        this.props.onApply(pcolor, scolor);
    }

    handleChangeComplete = (color) => {
        this.setState(color);
    }

    setColor = (color) => {
        this.setState({
            primaryColor: {
                r: color.primary[0],
                g: color.primary[1],
                b: color.primary[2],
            },
            secondaryColor: {
                r: color.secondary[0],
                g: color.secondary[1],
                b: color.secondary[2],
            }
        });
    }

    resetColor = () => {
        this.props.onResetColor();
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;


        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.onClose}
                    fullScreen={true}
                >
                    <DialogTitle>{this.props.title}</DialogTitle>
                    <DialogContent className={classes.dialogContent}>
                        <AppBar position="static" color="default">
                            <Tabs
                                value={this.state.value}
                                onChange={this.handleChange}
                                indicatorColor="primary"
                                textColor="primary"
                                fullWidth
                            >
                                <Tab label="Primary" />
                                <Tab label="Secondary" />
                            </Tabs>
                        </AppBar>
                        {value === 0 && <div>
                            <SketchPicker
                                color={this.state.primaryColor}
                                onChangeComplete={(color) => this.handleChangeComplete({ primaryColor: color.rgb })}
                                disableAlpha={true}
                                width={'calc(100vw - 36px'}
                            />
                        </div>}
                        {value === 1 && <div>
                            <SketchPicker
                                color={this.state.secondaryColor}
                                onChangeComplete={(color) => this.handleChangeComplete({ secondaryColor: color.rgb })}
                                disableAlpha={true}
                                width={'calc(100vw - 36px'}
                            />
                        </div>}
                        <Button
                                variant="outlined"
                                fullWidth
                                className={classes.button}
                                onClick={this.resetColor} 
                            >
                                Reset to default
                            </Button>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.onOk} color="primary">
                            Save
                        </Button>
                        <Button onClick={this.onApply} color="primary">
                            Apply
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(DuoColorPicker);