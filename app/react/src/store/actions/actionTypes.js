/*
 * File:			actionTypes.js
 * Project:			ltc-app
 * Created:			19.09.2018, 14:21:57
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:31:56
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


// websocket.js
export const WS_CONNECTED = 'WS_CONNECTED';
export const WS_DISCONNECTED = 'WS_DISCONNECTED';
export const WS_CONNECT = 'WS_CONNECT';
export const WS_CONNECTION_FAILED = 'WS_CONNECTION_FAILED';

// nodes.js
export const CMD_STATUS = 'CMD_STATUS';
export const CLEAR_NODES = 'CLEAR_NODES';
export const CLEAR_PARAMETER_FETCHED = 'CLEAR_PARAMETER_FETCHED';
export const CMD_PARA = 'CMD_PARA';
export const CMD_SETTINGS = 'CMD_SETTINGS'
export const CLEAR_SETTINGS_FETCHED = 'CLEAR_SETTINGS_FETCHED';