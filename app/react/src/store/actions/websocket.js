/*
 * File:			websocket.js
 * Project:			ltc-app
 * Created:			19.09.2018, 14:22:06
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:30:51
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import * as actionTypes from './actionTypes';
import { createWs, closeWs, sendBroadcast } from '../../utility/Sockette/Sockette';
import history from '../../utility/history/history';
import { handleCmd } from '../../utility/CmdHandler/CmdHandler';

let connectionTimeout, connected;

const wsConnected = ip => {
    return {
        type: actionTypes.WS_CONNECTED,
        ip: ip
    }
}

const wsDisconnected = () => {
    return {
        type: actionTypes.WS_DISCONNECTED
    }
}

const wsConnect = () => {
    //console.log("wsConnect()");
    return {
        type: actionTypes.WS_CONNECT
    }
}

const wsConnectionFailed = () => {
    //console.log("wsConnectionFailed()");
    return {
        type: actionTypes.WS_CONNECTION_FAILED
    }
}

export const connectWs = (ip) => {
    //console.log("connectWs()");
    return dispatch => {
        dispatch(wsConnect());

        setTimeout(() => {
            createWs(ip,
                e => onOpen(e, dispatch, ip),
                e => onMessage(e, dispatch),
                e => onReconnect(e, dispatch),
                e => onMaximum(e, dispatch),
                e => onClose(e, dispatch),
                e => onError(e, dispatch));
            
            connectionTimeout = setTimeout(() => {
                //console.log('timeout');
                closeWs();
            }, 5000);
        }, 5000);
    }
}

const resetTimeout = () => {
    clearTimeout(connectionTimeout);
}

//Websocket event handlers
const onOpen = (e, dispatch, ip) => {
    console.log('Connected!', e);
    dispatch(wsConnected(ip));
    resetTimeout();
    connected = true;
    history.push('/main');
    sendBroadcast('getStatus', {}, connected);
}

const onMessage = (e, dispatch) => {
    console.log('Received:', e);
    handleCmd(JSON.parse(e.data), dispatch);
}

const onReconnect = (e, dispatch) => {
    console.log('Reconnecting...', e);
}

const onMaximum = (e, dispatch) => {
    //console.log('Stop Attempting!', e);
    //dispatch(wsConnectionFailed());
}

const onClose = (e, dispatch) => {
    console.log('Closed!', e);
    dispatch(wsDisconnected());
    dispatch(wsConnectionFailed());
    if (connected) {
        connected = false;
    }
    history.push('/connect');    
}

const onError = (e, dispatch) => {
    console.log('Error:', e);
}