/*
 * File:			nodes.js
 * Project:			ltc-app
 * Created:			19.09.2018, 20:19:34
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import * as actionTypes from './actionTypes';
import { sendBroadcast, sendCmd } from '../../utility/Sockette/Sockette';

export const cmdStatus = (source, data) => {
    return {
        type: actionTypes.CMD_STATUS,
        source: source,
        data: data
    }
}

export const updateNodes = (connected) => {
    return dispatch => {
        dispatch(clearNodes());
        sendBroadcast('getStatus', {}, connected);
    }
}

const clearNodes = () => {
    return {
        type: actionTypes.CLEAR_NODES
    }
}

export const fetchParameter = (id, connected) => {
    return dispatch => {
        sendCmd(id, 'getPara', {}, connected);
        dispatch(clearParameterFetched(id));
    }
}

export const clearParameterFetched = (id) => {
    return {
        type: actionTypes.CLEAR_PARAMETER_FETCHED,
        id: id
    }
}

export const cmdPara = (source, data) => {
    return {
        type: actionTypes.CMD_PARA,
        source: source,
        data: data
    }
}

export const cmdSettings = (source, data) => {
    return {
        type: actionTypes.CMD_SETTINGS,
        source: source,
        data: data
    }
}

export const clearSettingsFetched = (id) => {
    return {
        type: actionTypes.CLEAR_SETTINGS_FETCHED,
        id: id
    }
}

export const fetchSettings = (id, connected) => {
    return dispatch => {
        sendCmd(id, 'getSettings', {}, connected);
        dispatch(clearSettingsFetched(id));
    }
}