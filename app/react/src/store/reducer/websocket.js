/*
 * File:			websocket.js
 * Project:			ltc-app
 * Created:			19.09.2018, 14:21:17
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:31:46
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../utility/updateObject/updateObject';

const initialState = {
    connected: false,
    autoIp: [],
    connectedIp: [],
    connectionFailed: true,
    address: "www.whooplights.esp"
}

const wsConnected = (state, action) => {
    return updateObject(state, {
        connectedIp: action.ip,
        connected: true
    });
}

const wsDisconnected = (state, action) => {
    return updateObject(state, {
        connected: false
    });
}

const wsConnect = (state, action) => {
    return updateObject(state, {
        connectionFailed: false
    });
}

const wsConnectionFailed = (state, action) => {
    return updateObject(state, {
        connectionFailed: true
    });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.WS_CONNECTED: return wsConnected(state, action);
        case actionTypes.WS_DISCONNECTED: return wsDisconnected(state, action);
        case actionTypes.WS_CONNECT: return wsConnect(state, action);
        case actionTypes.WS_CONNECTION_FAILED: return wsConnectionFailed(state, action);
            
        default:
            return state;
    }
}

export default reducer;