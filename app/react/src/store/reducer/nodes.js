/*
 * File:			nodes.js
 * Project:			ltc-app
 * Created:			19.09.2018, 20:19:27
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	07.10.2018, 11:51:53
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../utility/updateObject/updateObject';

const initialState = {
    nodes: [],
    settings: {
        numLeds: {
            name: "Number LEDs",
            description: [
                "Defines how many LEDs you strip has.",
                "Note: On WS2811 every real 3 LEDs counts to 1 LED in software (1m with 60LEDs/m has 20 LEDs in software)."
            ]
        },
        voltageScale: {
            name: "Voltage Scale",
            description: [
                "Internal multiplicator to calculate input voltage.",
                "If displayed voltage is off, calc new voltage scale with following formula:",
                "newScale = (realVoltage / displayedVoltage) * currentScale",
                "Note: Check if your caculated scale is near to the original. If it's way off you may caculated it wrong.",
                "Too low number can damage you LEDs if input power is above 12V!"
            ]
        },
        warningCellVoltage: {
            name: "Warning Voltage (per cell)",
            description: [
                "Voltage per cell in mV (so 3.6V will be 3600).",
                "Under this voltage the strip will start blinking red."
            ]
        },
        brightness: {
            name: "Brightness",
            description: [
                "User set brightness (0-255)"
            ]
        }
    }
}

const nodeTemplate = {
    id: 0,
    alias: '',
    effectId: 0,
    parameter: {
        color: {
            primary: [0,0,0],
            secondary: [0,0,0]
        },
        para: [0,0,0,0,0]
    },
    parameterFetched: false,
    settings: {
        numLeds: 0,
        voltage: 0.0,
        voltageScale: 0,
        warningCellVoltage: 0,
        brightness: 0,
    },
    settingsFetched: false
}

const cmdStatus = (state, action) => {
    let newNodes, newNode;
    const existingNodeIndex = state.nodes.findIndex(no => no.id === action.source);

    // copy existing array
    newNodes = JSON.parse(JSON.stringify(state.nodes));;

    // creating a deep copy of the template
    newNode = JSON.parse(JSON.stringify(nodeTemplate));
    
    // fill newNode with real values
    newNode.id = action.source;
    newNode.alias = action.data.alias;
    newNode.effectId = action.data.effectId;
    newNode.parameterFetched = false;

    if(existingNodeIndex === -1) {
        // the id isn't in the array yet
        newNodes.push(newNode);
    }
    else {
        // id exsists, just update the values
        newNodes[existingNodeIndex] = newNode;
    }

    newNodes.sort((a,b) => (a.alias < b.alias ? -1 : (a.alias > b.alias ? 1 : 0)));

    return updateObject(state, {nodes: newNodes});
}

const clearNodes = (state, action) => {
    return updateObject(state, {nodes: []});
}

const clearParameterFetched = (state, action) => {
    let newNodes = JSON.parse(JSON.stringify(state.nodes));;
    const existingNodeIndex = state.nodes.findIndex(no => no.id === action.id);
    if(existingNodeIndex !== -1) {
        newNodes[existingNodeIndex].parameterFetched = false;
        return updateObject(state, {nodes: newNodes});
    }
    else {
        return state;
    }
}

const cmdPara = (state, action) => {
    const existingNodeIndex = state.nodes.findIndex(no => no.id === action.source);
    
    if(existingNodeIndex === -1) {
        // something went wrong
        return state;
    }
    else {
        // id exsists, just update the values
        if(action.data.effectId !== state.nodes[existingNodeIndex].effectId) {
            // this can't be right, too
            return state;
        }
        else {
            let newNodes;
            // copy existing array
            newNodes = JSON.parse(JSON.stringify(state.nodes));;
            // primary color
            newNodes[existingNodeIndex].parameter.color.primary[0] = action.data.color.primary[0];
            newNodes[existingNodeIndex].parameter.color.primary[1] = action.data.color.primary[1];
            newNodes[existingNodeIndex].parameter.color.primary[2] = action.data.color.primary[2];
            // secondary color
            newNodes[existingNodeIndex].parameter.color.secondary[0] = action.data.color.secondary[0];
            newNodes[existingNodeIndex].parameter.color.secondary[1] = action.data.color.secondary[1];
            newNodes[existingNodeIndex].parameter.color.secondary[2] = action.data.color.secondary[2];
            //para
            for(let i = 0; i < 5; i++) {
                newNodes[existingNodeIndex].parameter.para[i] = action.data.para[i];
            }
            // set parameterFetched
            newNodes[existingNodeIndex].parameterFetched = true;

            return updateObject(state, {nodes: newNodes});
        }
    }
}

const cmdSettings = (state, action) => {
    const existingNodeIndex = state.nodes.findIndex(no => no.id === action.source);
    
    if(existingNodeIndex === -1) {
        // something went wrong
        return state;
    }
    else {
        let newNodes;
        // copy existing array (deep copy)
        newNodes = JSON.parse(JSON.stringify(state.nodes));
        // settings
        newNodes[existingNodeIndex].settings.numLeds = action.data.numLeds;
        newNodes[existingNodeIndex].settings.voltage = action.data.voltage;
        newNodes[existingNodeIndex].settings.voltageScale = action.data.voltageScale;
        newNodes[existingNodeIndex].settings.warningCellVoltage = action.data.warningCellVoltage;
        newNodes[existingNodeIndex].settings.brightness = action.data.brightness;
    
        // set settingsFetched
        newNodes[existingNodeIndex].settingsFetched = true;

        return updateObject(state, {nodes: newNodes});
    }
}

const clearSettingsFetched = (state, action) => {
    let newNodes = JSON.parse(JSON.stringify(state.nodes));
    const existingNodeIndex = state.nodes.findIndex(no => no.id === action.id);
    if(existingNodeIndex !== -1) {
        newNodes[existingNodeIndex].settingsFetched = false;
        return updateObject(state, {nodes: newNodes});
    }
    else {
        return state;
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CMD_STATUS: return cmdStatus(state, action);
        case actionTypes.CLEAR_NODES: return clearNodes(state, action);
        case actionTypes.CLEAR_PARAMETER_FETCHED: return clearParameterFetched(state, action);
        case actionTypes.CMD_PARA: return cmdPara(state, action);
        case actionTypes.CMD_SETTINGS: return cmdSettings(state, action);
        case actionTypes.CLEAR_SETTINGS_FETCHED: return clearSettingsFetched(state, action);
            
        default:
            return state;
    }
}

export default reducer;