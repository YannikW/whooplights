/*
 * File:			effects.js
 * Project:			ltc-app
 * Created:			20.09.2018, 10:52:49
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	08.10.2018, 17:58:18
 * Modified By:		Yannik Wertz
 * -----
 * Controll application for whoopLights
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


const initialState = {
    effects: [
        {
            name: "Rainbow",
            description: "A rainbow cycles around the gate. Speed and number of full rainbows around the gate can be changed.",
            para: [
                {
                    name: "Speed",
                    description: "Delay in ms between two frames. Smaller values means faster movement.",
                    enabled: true
                },
                {
                    name: "Number",
                    description: "Number of complete rainbows from beginning to end of the strip.",
                    enabled: true
                },
                {
                    name: "Direction",
                    description: "0 = normal, 1 = reversed.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Fire",
            description: "This looks like a flickering fire - More or less.. ;) Color, flickering speed and intensity can be changed.",
            para: [
                {
                    name: "Speed fixed",
                    description: "Minimum Delay in ms between two frames. Smaller values means faster flickering.",
                    enabled: true
                },
                {
                    name: "Speed random",
                    description: "Additional random (max) delay, will be added to fixed delay.",
                    enabled: true
                },
                {
                    name: "Flicker",
                    description: "Greater values means heavier flickering.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Tunnel",
            description: "This effect is used with multiple gates in a tunnel configuration. Set total number of gates and individual number in the tunnel in the parameter.",
            para: [
                {
                    name: "Timestep",
                    description: "Delay until the next gate in the tunnel light up.",
                    enabled: true
                },
                {
                    name: "Number of gates",
                    description: "Total number of gates.",
                    enabled: true
                },
                {
                    name: "Position",
                    description: "Individual position (first, second, third, ...).",
                    enabled: true
                },
                {
                    name: "Stay enabled mode",
                    description: "0 = off, 1 = on. If enabled the gate stays on and don't go off until cycle repeats.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Larson Scanner",
            description: "Classic larson scanner. Also known as KITT or Knightrider.",
            para: [
                {
                    name: "Speed",
                    description: "Delay in ms between two frames. Smaller values means faster movement.",
                    enabled: true
                },
                {
                    name: "Width",
                    description: "Width of the running light.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Snake",
            description: "Snake goes around the gate.",
            para: [
                {
                    name: "Speed",
                    description: "Delay in ms between two frames. Smaller values means faster movement.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Theater Chase",
            description: "Blinking funfair lights.",
            para: [
                {
                    name: "Speed",
                    description: "Delay in ms between two frames. Smaller values means faster movement.",
                    enabled: true
                },
                {
                    name: "On Leds",
                    description: "How many leds in an \"On\" block.",
                    enabled: true
                },
                {
                    name: "Off Leds",
                    description: "How many leds in an \"Off\" block.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Strobe",
            description: "Strobe effect.",
            para: [
                {
                    name: "Speed",
                    description: "Delay in ms between On/Off toggle. Smaller values means faster flashing.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        },
        {
            name: "Police",
            description: "Flashing police light.",
            para: [
                {
                    name: "Speed",
                    description: "Delay in ms between On/Off toggle. Smaller values means faster flashing.",
                    enabled: true
                },
                {
                    name: "Pause multiplier",
                    description: "Number of pause frames (each with the 'Speed' delay) between side change.",
                    enabled: true
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                },
                {
                    name: "",
                    description: "",
                    enabled: false
                }
            ]
        }
    ]
}


const reducer = (state = initialState, action) => {
    switch (action.type) {

            
        default:
            return state;
    }
}

export default reducer;