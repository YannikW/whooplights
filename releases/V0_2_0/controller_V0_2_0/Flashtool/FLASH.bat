@echo off
:: Greetings to user
echo.
echo whoopLights Flashtool V0.1
echo.
echo Installing whoopLights controller firmware V0.2.0
echo.
echo.
echo Available COM ports:


setlocal

:: wmic /format:list strips trailing spaces (at least for path win32_pnpentity)
for /f "tokens=1* delims==" %%I in ('wmic path win32_pnpentity get caption /format:list ^| find "COM"') do (
    call :setCOM "%%~J"
)

:: display all _COM* variables
set _COM

:: end main batch
goto :FLASH

:setCOM <WMIC_output_line>
:: sets _COM#=line
setlocal
set "str=%~1"
set "num=%str:*(COM=%"
set "num=%num:)=%"
set str=%str:(COM=&rem.%
endlocal & set "_COM%num%=%str%"
goto :EOF

:FLASH
echo.
echo.
echo Please enter the ESP8266 COM port number (search for CP210x in above list)
set /p COM=
esp_tool -a0x00000 -b115200 -ff80M -rnodeMCU -p%COM% firmware.bin

::Flashing completed
pause