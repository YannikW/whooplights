/*
 * File:			defaults.h
 * Project:			whoopLights
 * Created:			21.09.2018, 20:31:25
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	08.10.2018, 18:02:52
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * This are the default configurations.
 * A more readable version of this can be found at Gitlab: 
 * https://gitlab.com/YannikW/whoopLights/blob/master/docs/Configs.md
 * 
 */

#ifndef defaults_h
#define default_h

// settings
char *settingsDefaultJson = (char*)R"({"alias":{"version":[0,1,0],"value":""},"numLeds":{"version":[0,1,0],"value":20},"voltageScale":{"version":[0,1,0],"value":17661},"warningCellVoltage":{"version":[0,1,0],"value":3650},"brightness":{"version":[0,1,0],"value":255}})";

// effects
char *effectsDefaultJson = (char*)R"({"activeEffectId":{"version":[0,1,0],"value":0},"effects":[{"name":"Rainbow","version":[0,1,0],"color":{"primary":[0,0,0],"secondary":[0,0,0]},"para":[14,2,0,0,0]},{"name":"Fire","version":[0,1,0],"color":{"primary":[255,96,12],"secondary":[0,0,0]},"para":[30,160,70,0,0]},{"name":"Tunnel","version":[0,1,0],"color":{"primary":[255,0,40],"secondary":[0,30,30]},"para":[200,3,1,1,0]},{"name":"Larson Scanner","version":[0,1,0],"color":{"primary":[255,0,0],"secondary":[40,40,40]},"para":[30,5,0,0,0]},{"name":"Snake","version":[0,1,0],"color":{"primary":[255,70,0],"secondary":[0,70,70]},"para":[50,0,0,0,0]},{"name":"Theater Chase","version":[0,1,0],"color":{"primary":[77,0,255],"secondary":[200,200,0]},"para":[50,2,3,0,0]},{"name":"Strobe","version":[0,1,0],"color":{"primary":[255,255,255],"secondary":[0,0,0]},"para":[25,0,0,0,0]},{"name":"Police","version":[0,1,0],"color":{"primary":[255,0,0],"secondary":[0,0,255]},"para":[50,2,0,0,0]}]})";

/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif // defaults_h