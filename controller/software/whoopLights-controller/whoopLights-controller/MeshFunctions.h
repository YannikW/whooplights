/*
 * File:			MeshFunctions.h
 * Project:			whoopLights
 * Created:			19.08.2018, 16:55:54
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef _MESHFUNCTIONS_H
#define _MESHFUNCTIONS_H

/*#######################################################
##                      Includes                       ##
#######################################################*/

//Basic
#include "Arduino.h"

//External Libraries
#include "painlessMesh.h"
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"

//Internal Libraries
#include "CommandParser.h"

//User files / program separation
#include "config.h"


/*#######################################################
##                     Constants                       ##
#######################################################*/

/*#######################################################
##                     Externals                       ##
#######################################################*/

extern CommandParser meshParser;
extern AsyncWebSocket ws;


/*#######################################################
##                 Function prototypes                 ##
#######################################################*/


void receivedCallback(uint32_t from, String &msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback();
void nodeTimeAdjustedCallback(int32_t offset);
void delayReceivedCallback(uint32_t from, int32_t delay);


/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif //_MESHFUNCTIONS_H