/*
 * File:			Commands.cpp
 * Project:			whoopLights
 * Created:			13.09.2018, 20:57:37
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	08.10.2018, 18:17:51
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/*#######################################################
##                   Include Header                    ##
#######################################################*/

#include "Commands.h"


/*#######################################################
##                      Objects                        ##
#######################################################*/


/*#######################################################
##                     Variables                       ##
#######################################################*/


/*#######################################################
##                     Functions                       ##
#######################################################*/

/**
 * Create command dictionary
 */
void initCommands() {
    cmdList.addCommand("getPara", getPara);
    cmdList.addCommand("getSettings", getSettings);
    cmdList.addCommand("getStatus", getStatus);
    cmdList.addCommand("resetColor", resetColor);
    cmdList.addCommand("resetPara", resetPara);
    cmdList.addCommand("resetSettings", resetSettings);
    cmdList.addCommand("setAlias", setAlias);
    cmdList.addCommand("setColor", setColor);
    cmdList.addCommand("setEffect", setEffect);
    cmdList.addCommand("setPara", setPara);
    cmdList.addCommand("setSettings", setSettings);
}

/**
 * Initialize the parser
 */
void initParser() {
    meshParser.addCommandList(cmdList);
    meshParser.setId(mesh.getNodeId());
    wsParser.addCommandList(cmdList);
    wsParser.setId(mesh.getNodeId());
}


/*#######################################################
##                  Basic Functions                    ##
#######################################################*/

void sendCommand(JsonObject& root, uint8_t type, const char *cmd, uint32_t target) {
    // Add basic command structure
    root["type"] = type;
    root["source"] = mesh.getNodeId();
    if(type == TYPE_TARGET) {
        root["target"] = target;
    }
    root["command"] = cmd;

    //root.prettyPrintTo(Serial);  
    // Print to websocket and mesh
    String str;
    root.printTo(str);
    ws.printfAll(str.c_str());
    mesh.sendBroadcast(str);
}


/*#######################################################
##                 Command Callbacks                   ##
#######################################################*/

/**
 * Trigger to send a "para" command
 */
void getPara(JsonObject& data) {
    para(data["source"]);
}

/**
 * Trigger to send a "settings" command back to source
 */
void getSettings(JsonObject& data) {
    settings(data["source"]);
}

/**
 * Trigger to send a "status" command back to source
 */
void getStatus(JsonObject& data) {
    status(data["source"]);
}

/**
 * Reset colors t default
 */
void resetColor(JsonObject& data) {
    uint8_t effectId = data["effectId"];
    uint32_t source = data["source"];
    loadDefaultColor(effectId);

    para(source);
    enableSaveEffectsTask();
}

/**
 * Reset colors t default
 */
void resetPara(JsonObject& data) {
    uint8_t effectId = data["effectId"];
    uint32_t source = data["source"];
    loadDefaultPara(effectId);

    para(source);
    enableSaveEffectsTask();
}

/**
 * Reset settings
 */
void resetSettings(JsonObject& data) {
    uint32_t source = data["source"];
    loadDefaultSettings();

    settings(source);
    enableSaveSettingsTask();
}

/**
 * Set node alias
 */
void setAlias(JsonObject& data) {
    const char *name = data["name"];
    if(name) {
        strncpy(alias, name, (sizeof(alias) - 1));
        status(data["source"]);
        enableSaveSettingsTask();
    }
}

/**
 * Set colors
 */
void setColor(JsonObject& data) {
    uint8_t effectId = data["effectId"];

    // Primary Color
    effects[effectId].color[0].R = data["color"]["primary"][0];
    effects[effectId].color[0].G = data["color"]["primary"][1];
    effects[effectId].color[0].B = data["color"]["primary"][2];

    // Secondary color
    effects[effectId].color[1].R = data["color"]["secondary"][0];
    effects[effectId].color[1].G = data["color"]["secondary"][1];
    effects[effectId].color[1].B = data["color"]["secondary"][2];

    para(data["source"]);
    enableSaveEffectsTask();
}

/**
 * Set active effect
 */
void setEffect(JsonObject& data) {
    setNextEffect(data["effectId"]);
    status(data["source"]); 
    enableSaveEffectsTask();
}

/**
 * Set parameter 
 */
void setPara(JsonObject& data) {
    uint8_t effectId = data["effectId"];

    // Parameter
    for(uint8_t i = 0; i < 5; i++) {
        effects[effectId].para[i] = data["para"][i];
    }

    para(data["source"]);
    enableSaveEffectsTask();
}

/**
 * Set settings 
 */
void setSettings(JsonObject& data) {
    
    voltageScale = data["voltageScale"];
    warningCellVoltage = data["warningCellVoltage"];
    brightness = data["brightness"];
    // this trigger new brightness set
    readVoltage();

    uint16_t newNumLeds = data["numLeds"];
    if(newNumLeds != numLeds) {
        numLeds = newNumLeds;
        initLeds(newNumLeds);
    }

    settings(data["source"]);
    enableSaveSettingsTask();
}


/*#######################################################
##              Client -> Master Commands              ##
#######################################################*/

/**
 * Send a para frame
 */
void para(uint32_t target) {
    // Create JSON buffer and object
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& data = root.createNestedObject("data");

    // Fill in para data
    data["effectId"] = nextEffectId;

    // create color object
    JsonObject& color = data.createNestedObject("color");
    // create array for primary color
    JsonArray& primary = color.createNestedArray("primary");
    primary.add(effects[nextEffectId].color[0].R);
    primary.add(effects[nextEffectId].color[0].G);
    primary.add(effects[nextEffectId].color[0].B);
    // create array for secondary color
    JsonArray& secondary = color.createNestedArray("secondary");
    secondary.add(effects[nextEffectId].color[1].R);
    secondary.add(effects[nextEffectId].color[1].G);
    secondary.add(effects[nextEffectId].color[1].B);

    // create array for para
    JsonArray& paraArr = data.createNestedArray("para");
    for(uint8_t i = 0; i < 5; i++) {
        paraArr.add(effects[nextEffectId].para[i]);
    }

    // Send command
    sendCommand(root, TYPE_TARGET, "para", target);
}

/**
 * Send a settings frame
 */
void settings(uint32_t target) {
    // Create JSON buffer and object
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& data = root.createNestedObject("data");

    // Fill in settings
    data["numLeds"] = numLeds;
    data["voltageScale"] = voltageScale;
    data["warningCellVoltage"] = warningCellVoltage;
    readVoltage();
    double realVolt = voltage / 1000.0; //convert from mV to volt
    data["voltage"] = realVolt;
    data["brightness"] = brightness;

    // Send command
    sendCommand(root, TYPE_TARGET, "settings", target);
}

/**
 * Send a status frame
 */
void status(uint32_t target) {
    // Create JSON buffer and object
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& data = root.createNestedObject("data");

    // Fill in status data
    data["effectId"] = nextEffectId;    // not activeEffectId, because it will take one cycle before it's updated.. it's no pretty but works
    data["alias"] = alias;

    // Send command
    sendCommand(root, TYPE_TARGET, "status", target);
}


/*#######################################################
##                    End of file                      ##
#######################################################*/