/*
 * File:			  MeshFunctions.cpp
 * Project:			whoopLights
 * Created:			19.08.2018, 16:55:54
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/*#######################################################
##                   Include Header                    ##
#######################################################*/

#include "MeshFunctions.h"


/*#######################################################
##                      Objects                        ##
#######################################################*/

extern painlessMesh mesh;
extern SimpleList<uint32_t> nodes;


/*#######################################################
##                     Variables                       ##
#######################################################*/


/*#######################################################
##                      Functions                      ##
#######################################################*/

/** This function is called every time a message from the mesh is received
 * 
 * Param:
 *  from:   ID of the sender
 *  msg:    message received
 * 
 * Return:
 *  -
 */
void receivedCallback(uint32_t from, String & msg) {
  #ifdef MSG_RECEIVED_DEBUG 
    Serial.printf("receivedCallback(): Received from %u msg=%s\n", from, msg.c_str());
  #endif

  // Parse message
  meshParser.parse((char*)msg.c_str());

  // Forward message to websocket
  ws.printfAll(msg.c_str());
}

/** This fires every time the local node makes a new connection.
 * 
 * Param:
 *  nodeId:   ID of the sender
 * 
 * Return:
 *  -
 */
void newConnectionCallback(uint32_t nodeId) {
  #ifdef NEW_CONNECTION_DEBUG
    Serial.printf("newConnectionCallback(): New Connection, nodeId = %u\n", nodeId);
  #endif
}

/** This fires every time there is a change in mesh topology.
 * 
 * Param:
 * 
 * Return:
 *  -
 */
void changedConnectionCallback() {
  #ifdef CHANGED_CONNECTION_DEBUG
    Serial.printf("changedConnectionCallback(): Changed connections %s\n", mesh.subConnectionJson().c_str());
  
    nodes = mesh.getNodeList();

    Serial.printf("changedConnectionCallback(): Num nodes: %d\n", nodes.size());
    Serial.printf("changedConnectionCallback(): Connection list:");

    SimpleList<uint32_t>::iterator node = nodes.begin();
    while (node != nodes.end()) {
      Serial.printf(" %u", *node);
      node++;
    }
    Serial.println();
  #endif
}

/** This fires every time local time is adjusted to synchronize it with mesh time.
 * 
 * Param:
 *  offset:   adjustment delta that has benn calculated and applied to local clock
 * 
 * Return:
 *  -
 */
void nodeTimeAdjustedCallback(int32_t offset) {
  #ifdef NODE_TIME_ADJ_DEBUG
    Serial.printf("nodeTimeAdjustedCallback(): Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
  #endif
}

/** This fires every time there is a change in mesh topology. Callback has the following structure.
 * 
 * Param:
 *  nodeId:   The node that originated response.
 *  delay:    One way network trip delay in microseconds.
 * 
 * Return:
 *  -
 */
void delayReceivedCallback(uint32_t from, int32_t delay) {
#ifdef DELAY_RECEIVED_DEBUG
  Serial.printf("delayReceivedCallback(): Delay to node %u is %d us\n", from, delay);
#endif
}

/*#######################################################
##                    End of file                      ##
#######################################################*/