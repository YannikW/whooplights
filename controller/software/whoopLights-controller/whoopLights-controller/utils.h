/*
 * File:			utils.h
 * Project:			whoopLights
 * Created:			27.08.2018, 20:41:09
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/*#######################################################
##                      Defines                        ##
#######################################################*/

/**
 * Subtract a - b.
 * Returned value will not be negativ (underflow protection).
 * Example: 10-5 returns 5. 5-10 returns 0 (NOT -5)
 */
#define sub_wo_underflow(a, b)  (a - min(a,b))