/*
 * File:			config.h
 * Project:			whoopLights
 * Created:			19.08.2018, 16:55:54
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:23:33
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef config_h
#define config_h


/*#######################################################
##                        Mesh                         ##
#######################################################*/

// Network
// Do not change things here without a reason
#define MESH_SSID       "whoopLights"
#define MESH_PASSWORD   "12345678"
#define MESH_PORT       5555

// DNS address
#define DNS_ADDRESS     "www.whooplights.esp"


// Mesh debugging
// Comment lines to disable debug outputs
#define MSG_RECEIVED_DEBUG
#define NEW_CONNECTION_DEBUG
#define CHANGED_CONNECTION_DEBUG
//#define NODE_TIME_ADJ_DEBUG
//#define DELAY_RECEIVED_DEBUG


/*#######################################################
##                      Effects                        ##
#######################################################*/

// Effects debugging
// Comment lines to disable debug outputs
//#define EFFECTS_DEBUG



/*#######################################################
##                      Voltage                        ##
#######################################################*/

#define VOLTAGE_READ_DELAY 5            // seconds
#define CELL_THRESHOLD 4400             //4.40V (eg. * 3 = 13.2V -> bigger is 4S, smaller 3S)
#define NUM_VOLTAGE_UNDER_WARNING 4

// Comment next line to disable voltage print
//#define PRINT_VOLTAGE

/*#######################################################
##                       SPIFFS                        ##
#######################################################*/

// Comment next line to disable SPIFFS debug outputs
//#define DEBUG_SPIFFS

/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif // config_h