/*
 * File:			Effects.cpp
 * Project:			whoopLights
 * Created:			19.08.2018, 16:55:54
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	08.10.2018, 18:20:06
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/*#######################################################
##                   Include Header                    ##
#######################################################*/

#include "Effects.h"

/*#######################################################
##                      Objects                        ##
#######################################################*/

//NeoPixel Strip
NeoPixelBrightnessBus<NeoBrgFeature, NeoEsp8266Uart800KbpsMethod>* strip = NULL; // create object dynamic


/*#######################################################
##                       Tasks                         ##
#######################################################*/

// Effects
Task effectTasks[numEffects];


/*#######################################################
##                     Variables                       ##
#######################################################*/

effect_t effects[numEffects];   // effect parameters, colors, ...
uint16_t activeEffectId;        // id of the currently active effect
uint16_t nextEffectId;          // id of the effect which will be enabled in next iteration, controlled external


/*#######################################################
##                      Functions                      ##
#######################################################*/

/** Fetch last effect settings from EEPROM (TODO) or initialize with default settings
 * 
 * Param:
 * 
 * Return:
 *  -
 */
void initializeEffects() {
    for (uint8_t i = 0; i < numEffects; i++) {
        // copy values from static declation to runtime effects object
        effects[i].name = effectCallbacks[i].name;
        
        //Create tasks
        effectTasks[i].setCallback(effectCallbacks[i].function);
        userScheduler.addTask(effectTasks[i]);
    }

    // add warning task to sheduler (but don't enable)
    userScheduler.addTask(taskWarning);

    #ifdef EFFECTS_DEBUG
        Serial.println("initializeEffects(): completet");
    #endif

    enableEffect();
}

/** This is called once on setup to initialize the leds
 * 
 * Param:
 * 
 * Return:
 *  -
 */
void initLeds(uint8_t num) {
    // Initialize strip
    changePixelCount(num);
}

/** This is called once on setup to initialize the leds
 * 
 * Param:
 *  n:      Number of LEDs in the strip (on WS2811 three LEDs are handeled as one LED in software)
 * 
 * Return:
 *  -
 */
void changePixelCount(uint16_t n) {
    // check if there is already a strip object and delete it
    if(strip != NULL) {
        delete strip;
    }
    // create and initialize new NeoPixelBus object
    strip = new NeoPixelBrightnessBus<NeoBrgFeature, NeoEsp8266Uart800KbpsMethod>(n);
    strip->SetBrightness(180);  //Save for 17V input voltage
    strip->Begin();
}

/** Enable one effect with updated parameters. Enable is synced with mesh time.
 * 
 * Param:
 *  id:     ID of the effect which will be enabled
 * 
 * Return:
 *  -
 */
void enableEffect(uint16_t extIterations) {
    // if we changed the effect we wanna start with 1 iteration 
    // (to get correct iterations back at next enableFunction call)
    uint16_t iterations;
    if(activeEffectId == nextEffectId) {
        // use the given iterations if we're still in the same effect
        iterations = extIterations;
    }
    else {
        // start with one iterations
        // this we go very fast, so we get back the real iterations on next enable call
        iterations = 1;
    }

    for(uint8_t a = 0; a < numEffects; a++) {
        if(a==nextEffectId) {
            uint16_t interval = effects[a].para[0];
            uint32_t enDelay;
            if(iterations != 0) {
                // iteration = 0 mean interval is set by the effect function
                effectTasks[a].setInterval(interval);
                effectTasks[a].setIterations(iterations);
                // calc delay to sync task with mesh timebase
                enDelay = (interval*iterations) - (mesh.getNodeTime() % (interval*iterations*1000))/1000;
                effectTasks[a].enableDelayed(enDelay);
            }
            else {
                // iterations == 0
                effectTasks[a].setIterations(1);
                effectTasks[a].enableDelayed(); // use internal interval to enable
            }
            
            #ifdef EFFECTS_DEBUG
                Serial.printf("enableEffects(): Enabled effect %u with interval %u ms and %u iterations\n", a, interval, iterations);
            #endif
        }
        else {
            effectTasks[a].disable();
        }
    }

    activeEffectId = nextEffectId;
}

/**
 * Set the next effect and call enableEffect
 */
void setNextEffect(uint16_t id) {
    if(id < numEffects) {
        nextEffectId = id;
    }
    else {
        nextEffectId = numEffects-1;
    } 
}

/**
 * Disable all effects (eg. in case of undervoltage)
 */
void disableEffects() {
    // Disable all effect tasks
    for(uint8_t a = 0; a < numEffects; a++) {
        effectTasks[a].disable();
    }

    // Turn leds off
    setStripColor(RgbColor(0,0,0), true);
}

/**
 * Calculate brightness to protect strip from overvoltage
 */
void calcBrightness(uint16_t voltage) {
    uint8_t outputBrightness;
    if(voltage < 12000) {
        // Full power if we're under 12V
        // use user setting for brightness
        outputBrightness = brightness;
    }
    else {
        outputBrightness = (uint8_t)(((12000.0/voltage)*255.0)*(brightness/255.0));
    }
    strip->SetBrightness(outputBrightness);
    #ifdef EFFECTS_DEBUG
        Serial.printf("calcBrightness(): Set brightness to %u\n", outputBrightness);
    #endif
}

/**
 * Set the whole Strip in one color.
 * Optional show() is executed.
 */
void setStripColor(RgbColor color, boolean show) {
    for(uint16_t i = 0; i < strip->PixelCount(); i++) {
        strip->SetPixelColor(i, color);
    }

    if(show) {
        strip->Show();
    }
}

/**
 * This task is active in warning mode (eg. if undervoltage)
 * It will run forever until reboot
 */
void warning() {
    if(taskWarning.getRunCounter() % 2) {
        setStripColor(RgbColor(255,0,0), true);
    }
    else {
        setStripColor(RgbColor(0,0,0), true);
    }
}

/**
 * Set the color of a part of the strip
 * Optional show() is executed.
 */
void setStripColorFromTo(RgbColor color, uint16_t from, uint16_t to, boolean show) {
    for(uint16_t i = from; i <= to; i++) {
        if(i < strip->PixelCount()) {
            strip->SetPixelColor(i, color);
        }
    }

    if(show) {
        strip->Show();
    }
}


/*#######################################################
##                  Effect Functions                   ##
#######################################################*/

/** Rainbow, which cycles around the strip
 * 
 * Param:
 *  -
 * 
 * Return:
 *  -
 */
void rainbow() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter();

    for(uint16_t i = 0; i < strip->PixelCount(); i++) {
        uint16_t idx = i;
        if(effects[activeEffectId].para[2]) 
            idx = (strip->PixelCount() - i - 1);    //reverse
        /**
         * Explanation of calculation parts to get the wheelposition:
         * 
         * runCount*2 : 
         *      this gives a shift by time.
         *      *2 because without the interval gets very small,
         *      which is causing unnecessary high framerate
         * 
         * i*255.0/(strip->PixelCount() - 1):
         *      i/PixelCount()-1 is a value beween 0.0 and 1.0,
         *      multiplied with 255 we got all 255 colors of the wheel
         *      one time around the strip at one frame.
         * 
         * effects[activeEffectId].para[1]:
         *      this multiplies the above number.
         *      eg. for 2 the above number get doubled and we reach the
         *      end of the wheel at half of the strip length.
         *      Or in other words: We have tweo rainbows around the strip.
         */ 
        strip->SetPixelColor(idx, wheel((uint16_t)(runCount*2+(effects[activeEffectId].para[1]*i*255.0/(strip->PixelCount() - 1)))%256));
    }

    strip->Show();

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        // Rainbow has always 128 iterations, (weel has 256 pos, but counter is doubled)
        enableEffect(128);
    }
}

/** 
 * Rainbow wheel, this return a color depending on the given wheelPos
 * It cycles from red to green to blue and back to red
 * 
 * Param:
 *  wheelPos:   index from 0-255
 * 
 * Return:
 *  RgbColor:   Color object
 */
RgbColor wheel(uint8_t wheelPos) {
    if(wheelPos < 85) {
        return RgbColor(wheelPos * 3, 255 - wheelPos * 3, 0);
    } 
    else if(wheelPos < 170) {
        wheelPos -= 85;
        return RgbColor(255 - wheelPos * 3, 0, wheelPos * 3);
    } 
    else {
        wheelPos -= 170;
        return RgbColor(0, wheelPos * 3, 255 - wheelPos * 3);
  }
}

/** Flickering fire simulation
 * 
 * Param:
 *  -
 * 
 * Return:
 *  -
 */
void fire() {
    uint8_t flicker, r, g, b;

    for(uint16_t i = 0; i < strip->PixelCount(); i++) {
        flicker = ESP8266TrueRandom.random(effects[activeEffectId].para[2]);
        r = sub_wo_underflow(effects[activeEffectId].color[0].R, flicker);
        g = sub_wo_underflow(effects[activeEffectId].color[0].G, flicker);
        b = sub_wo_underflow(effects[activeEffectId].color[0].B, flicker);


        strip->SetPixelColor(i, RgbColor(r, g, b));
    }

    strip->Show();

    effectTasks[activeEffectId].setInterval(effects[activeEffectId].para[0] + ESP8266TrueRandom.random(effects[activeEffectId].para[1]));

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        // this effect is only one iteration, but
        // we dont wanne set interval by enableEffect, so pass 0
        enableEffect(0);
    }
}

/**
 * Tunnel, used with multiple gates
 * para[0]: Delay between to frames
 * para[1]: Total number of frames (gates)
 * para[2]: My position in the tunnel, from beginning to end (or reversed if you like)
 * para[3]: Stay enabled: If this is true the gate will stay enabled until the end of the cycle.
 *                        If false this gate will only lit if counter equals my position and go off after.  
 * 
 * primary color: Color when the gate is on
 * secondary color: Color when the gate is off
 */
void tunnel() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter() - 1;
    uint16_t totalFrames = effects[activeEffectId].para[1] + 1;
    uint16_t myPosition = effects[activeEffectId].para[2];
    uint16_t stayEnabled = effects[activeEffectId].para[3];
    RgbColor primary = effects[activeEffectId].color[0];
    RgbColor secondary = effects[activeEffectId].color[1];
    
    if(runCount == myPosition) {
        // gate is on
        setStripColor(primary, false);
    }
    else if(runCount > myPosition && stayEnabled) {
        // in stayEnabled mode it stays on
        setStripColor(primary, false);
    }
    else {
        // gate is off
        setStripColor(secondary, false);
    }

    // show the strip
    strip->Show();

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        enableEffect(totalFrames);
    }

}

/**
 * Classic larson scanner
 * para[0]: Delay between to frames
 * para[1]: Width of the scanner

 * 
 * primary color: Color of the scanner
 * secondary color: Background color
 */
void larsonScanner() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter();
    uint16_t pixelCount = strip->PixelCount();
    uint16_t width = effects[activeEffectId].para[1];
    RgbColor primary = effects[activeEffectId].color[0];
    RgbColor secondary = effects[activeEffectId].color[1];

    // Reset whole strip
    setStripColor(secondary, false);

    // Calculate position
    uint16_t position;
    if(runCount <= pixelCount) {
        // Run from start to end
        position = runCount-1;  // 0 to pixelCount-1
    }
    else {
        // Run from end to start
        position = pixelCount - (runCount - pixelCount);    // pixelCount-1 to 0
    }

    // Check for even number (we need uneven)
    if(!(width % 2)) {
        // even
        width--;    //make it an uneven number
    }

    //set middle pixel
    strip->SetPixelColor(position, primary);
    // Set left and right pixels
    for(uint16_t offset = 1; offset <= (width/2); offset++) {
        uint8_t darkenStep = 255 / (width/2);
        primary.Darken(darkenStep);
        int16_t offsetPosition = position + offset;
        if(offsetPosition < pixelCount) {
            strip->SetPixelColor(offsetPosition, primary);
        }
        offsetPosition = position - offset;
        if(offsetPosition >= 0) {
            strip->SetPixelColor(offsetPosition, primary);
        }
    }

    strip->Show();


    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        enableEffect(pixelCount* 2);
    }
}

/**
 * Snake goes around the gate.
 * para[0]: Delay between to frames
 * 
 * primary color: Head of the snake
 * secondary color: Tail of the snake
 */
void snake() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter();
    uint16_t pixelCount = strip->PixelCount();
    RgbColor primary = effects[activeEffectId].color[0];
    RgbColor secondary = effects[activeEffectId].color[1];

    for(uint16_t i = 0; i < pixelCount; i++) {
        double progress = (double)i / (pixelCount-1);
        RgbColor result = RgbColor::LinearBlend(primary, secondary, progress);

        strip->SetPixelColor(i, result);
    }

    strip->RotateLeft(runCount-1);

    strip->Show();

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        enableEffect(pixelCount);
    }
}

/**
 * Theater Chase
 * para[0]: Delay between to frames
 * para[1]: On leds
 * para[2]: Off leds
 * 
 * primary color: On pixels.
 * secondary color: Off pixels.
 */
void theaterChase() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter();
    uint16_t pixelCount = strip->PixelCount();
    uint16_t onPixel = effects[activeEffectId].para[1];
    uint16_t offPixel = effects[activeEffectId].para[2];
    RgbColor primary = effects[activeEffectId].color[0];
    RgbColor secondary = effects[activeEffectId].color[1];

    uint16_t i = 0;
    while (i < pixelCount) {
        // Set on pixels
        for(uint8_t j = 0; j < onPixel; j++) {
            if(i < pixelCount) {
                strip->SetPixelColor(i, primary);
                i++;
            }
        }
        // Set off pixels
        for(uint8_t j = 0; j < offPixel; j++) {
            if(i < pixelCount) {
                strip->SetPixelColor(i, secondary);
                i++;
            }
        }
    }

    strip->RotateLeft(runCount-1);

    strip->Show();

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        enableEffect(onPixel+offPixel);
    }
}

/**
 * Strobe effect.
 * para[0]: Delay between to frames
 * 
 * primary color: Strobe color
 * secondary color: 
 */
void strobe() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter();
    RgbColor primary = effects[activeEffectId].color[0];

    if(runCount == 1) {
        setStripColor(primary, true);
    }
    else {
        setStripColor(RgbColor(0,0,0), true);
    }

    strip->Show();

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        enableEffect(2);
    }
}

/**
 * Police lights flashing effect
 * para[0]: Delay between to frames
 * para[1]: Number of pause frames after tripple flash
 * 
 * primary color: Double flash color (first two flashed)
 * secondary color: Single flash color (third flash)
 */
void police() {
    uint16_t runCount = effectTasks[activeEffectId].getRunCounter() - 1;
    RgbColor primary = effects[activeEffectId].color[0];
    RgbColor secondary = effects[activeEffectId].color[1];
    uint16_t pixelCount = strip->PixelCount();
    uint16_t pauseFrames = effects[activeEffectId].para[1];
    uint16_t totalFrames = 5 + pauseFrames;

    uint16_t lowerBound, upperBound;

    if(runCount < totalFrames) {
        // First side flashing
        lowerBound = 0;
        upperBound = (pixelCount/2) - 1;
    }
    else {
        // Second side flashing
        lowerBound = pixelCount/2 + pixelCount%2;
        upperBound = pixelCount - 1;
    }

    switch(runCount % totalFrames) {
        case 0:
        case 2:
            setStripColor(RgbColor(0,0,0), false);
            setStripColorFromTo(primary, lowerBound, upperBound, true);
            break;
        case 1:
        case 3:
        case 5:
            setStripColor(RgbColor(0,0,0), true);
            break;
        case 4:
            setStripColor(RgbColor(0,0,0), false);
            setStripColorFromTo(secondary, lowerBound, upperBound, true);
            break;
    }

    // Check if cycle is completet and restart task (or start other task if mode has changed) in sync with the mesh
    if(effectTasks[activeEffectId].isLastIteration()) {
        enableEffect(totalFrames*2);
    }
}

/*#######################################################
##                    End of file                      ##
#######################################################*/