/*
 * File:			Filesystem.cpp
 * Project:			whoopLights
 * Created:			22.09.2018, 19:21:35
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	07.10.2018, 11:26:21
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/*#######################################################
##                   Include Header                    ##
#######################################################*/

#include "Filesystem.h"


/*#######################################################
##                      Objects                        ##
#######################################################*/


/*#######################################################
##                     Variables                       ##
#######################################################*/


/*#######################################################
##                     Functions                       ##
#######################################################*/

/**
 * Initialize the SPIFFS
 */
void initSPIFFS() {
    if (!SPIFFS.begin()) {
        Serial.printf("initSPIFFS(): Failed to mount file system\n");
    }
    else {
        Serial.printf("initSPIFFS(): Mount file system ok\n");
    }
}

/**
 * Fetch settings from user config files in SPIFFS or use defaults.
 */
void fetchSettingsConfig() {
    // Create Buffer
    DynamicJsonBuffer bufferSettingsDefault;
    // Try to parse default config, force duplicate of settingsDefaultJson
    JsonObject& rootSettingsDefault = bufferSettingsDefault.parseObject((const char*)settingsDefaultJson);
    // Check for parsing succeeds
    if(!rootSettingsDefault.success()) {
        Serial.printf("fetchSettingsConfig(): Parsing default root object failed, stop program\n");
        // endless loop, this error can't be corrected.
        while(true) {};
    }
    else {
        #ifdef DEBUG_SPIFFS
            Serial.printf("fetchSettingsConfig(): Parsing default root object ok\n");
            rootSettingsDefault.prettyPrintTo(Serial);
            Serial.println();
        #endif
    }

    boolean validUserFile = true;
    // Try loading user settings config file
    File settingsConfigFile = SPIFFS.open("/settings.json", "r");
    // check for error
    if(!settingsConfigFile) {
        Serial.printf("fetchSettingsConfig(): No user settings config found\n");
        validUserFile = false;
    } 
    else {
        size_t size = settingsConfigFile.size();
        if (size > 1024) {
            Serial.printf("fetchSettingsConfig(): Settings config file size is too large\n");
            validUserFile = false;
        }
    }
    // Try to parse JSON in user file
    size_t size = settingsConfigFile.size();
    // Allocate a buffer to store contents of the file.
    std::unique_ptr<char[]> settingsUserJson(new char[size]);
    // We don't use String here because ArduinoJson library requires the input
    // buffer to be mutable. If you don't use ArduinoJson, you may as well
    // use configFile.readString instead.
    settingsConfigFile.readBytes(settingsUserJson.get(), size);

    // Create Buffer
    DynamicJsonBuffer bufferSettingsUser;
    // Try to parse user config
    JsonObject& rootSettingsUser = bufferSettingsUser.parseObject(settingsUserJson.get());
    // Check for parsing succeeds
    if(!rootSettingsUser.success()) {
        Serial.printf("fetchSettingsConfig(): Parsing user root object failed, use defaults\n");
        validUserFile = false;
    }
    else {
        #ifdef DEBUG_SPIFFS
            Serial.printf("fetchSettingsConfig(): Parsing user root object ok\n");
            rootSettingsUser.prettyPrintTo(Serial);
            Serial.println();
        #endif
    }

    /**
     * ------ Alias ------
     */
    uint8_t aliasUseDefault = true;
    if(validUserFile && rootSettingsUser.containsKey("alias")) {
        // check if version in config is greater or equal the needed version for this software version
        JsonObject& aliasUser = rootSettingsUser["alias"];
        JsonObject& aliasDefault = rootSettingsDefault["alias"];
        if(checkVersion(aliasUser, aliasDefault)) {
            aliasUseDefault = false;

            // Fetch real data
            const char *name = aliasUser["value"];
            if(name) {
                strncpy(alias, name, (sizeof(alias) - 1));
            }
        }
    }
    if(aliasUseDefault) {
        // Fetch defaults
        sprintf(alias,"%u", mesh.getNodeId());
    }

    /**
     * ------ NumLeds ------
     */
    uint8_t numLedsUseDefault = true;
    if(validUserFile && rootSettingsUser.containsKey("numLeds")) {
        // check if version in config is greater or equal the needed version for this software version
        JsonObject& numLedsUser = rootSettingsUser["numLeds"];
        JsonObject& numLedsDefault = rootSettingsDefault["numLeds"];
        if(checkVersion(numLedsUser, numLedsDefault)) {
            numLedsUseDefault = false;

            // Fetch real data
            numLeds = rootSettingsUser["numLeds"]["value"];
        }
    }
    if(numLedsUseDefault) {
        // Fetch defaults
        numLeds = rootSettingsDefault["numLeds"]["value"];
    }

    /**
     * ------ VoltageScale ------
     */
    uint8_t voltageScaleUseDefault = true;
    if(validUserFile && rootSettingsUser.containsKey("voltageScale")) {
        // check if version in config is greater or equal the needed version for this software version
        JsonObject& voltageScaleUser = rootSettingsUser["voltageScale"];
        JsonObject& voltageScaleDefault = rootSettingsDefault["voltageScale"];
        if(checkVersion(voltageScaleUser, voltageScaleDefault)) {
            voltageScaleUseDefault = false;

            // Fetch real data
            voltageScale = rootSettingsUser["voltageScale"]["value"];
        }
    }
    if(voltageScaleUseDefault) {
        // Fetch defaults
        voltageScale = rootSettingsDefault["voltageScale"]["value"];
    }

    /**
     * ------ WarningCellVoltage ------
     */
    uint8_t warningCellVoltageUseDefault = true;
    if(validUserFile && rootSettingsUser.containsKey("warningCellVoltage")) {
        // check if version in config is greater or equal the needed version for this software version
        JsonObject& warningCellVoltageUser = rootSettingsUser["warningCellVoltage"];
        JsonObject& warningCellVoltageDefault = rootSettingsDefault["warningCellVoltage"];
        if(checkVersion(warningCellVoltageUser, warningCellVoltageDefault)) {
            warningCellVoltageUseDefault = false;

            // Fetch real data
            warningCellVoltage = rootSettingsUser["warningCellVoltage"]["value"];
        }
    }
    if(warningCellVoltageUseDefault) {
        // Fetch defaults
        warningCellVoltage = rootSettingsDefault["warningCellVoltage"]["value"];
    }

    /**
     * ------ Brightness ------
     */
    uint8_t brightnessUseDefault = true;
    if(validUserFile && rootSettingsUser.containsKey("brightness")) {
        // check if version in config is greater or equal the needed version for this software version
        JsonObject& brightnessUser = rootSettingsUser["brightness"];
        JsonObject& brightnessDefault = rootSettingsDefault["brightness"];
        if(checkVersion(brightnessUser, brightnessDefault)) {
            brightnessUseDefault = false;

            // Fetch real data
            brightness = rootSettingsUser["brightness"]["value"];
        }
    }
    if(brightnessUseDefault) {
        // Fetch defaults
        brightness = rootSettingsDefault["brightness"]["value"];
    }

    Serial.printf("fetchSettingsConfig(): Settings loaded\n");
}

/**
 * Compare two versions by passing two object which contains a version key
 * Check for first >= second
 */
boolean checkVersion(JsonObject& first, JsonObject& second) {
    // get two version arrays
    JsonArray& firstJsonArray = first["version"];
    JsonArray& secondJsonArray = second["version"];

    // convert to uint8_t arrays
    uint8_t fristArray[3], secondArray[3];
    firstJsonArray.copyTo(fristArray);
    secondJsonArray.copyTo(secondArray);

    //compare arrays
    return isVersionGreaterOrEqual(fristArray, secondArray);
}

/**
 * Check if the version in first parameter (myVersion) is greater (or equal) then in second parameter (checkVersion)
 * If yes, return true, if not false.
 */
boolean isVersionGreaterOrEqual(uint8_t *myVersion, uint8_t *checkVersion) {
    if(myVersion[0] > checkVersion[0]) {
        // major is greater -> true
        return true;
    }
    else if(myVersion[0] == checkVersion[0]) {
        // major is equal, check minor
        if(myVersion[1] > checkVersion[1]) {
            // major is equal, but minor is greater -> true
            return true;
        }
        else if(myVersion[1] == checkVersion[1]) {
            // major and minor are equal, check patch
            if(myVersion[2] > checkVersion[2]) {
                // major and minor are equal, but patch is greater -> true
                return true;
            }
            else if(myVersion[2] == checkVersion[2]) {
                // major, minor and patch are equal -> true
                return true;
            }
            else {
                // major and minor is equal, but patch is smaller -> false
                return false;
            }
        }
        else {
            // major is equal, but minor is smaller -> false
            return false;
        }
    }
    else {
        // major is smaller -> false
        return false;
    }
}

/**
 * Save the settings config to SPIFFS
 */
void saveSettingsConfig() {
    // Create Buffer
    DynamicJsonBuffer bufferSettings;
    // Create root object
    JsonObject& root = bufferSettings.createObject();

    // Put data inside

    // Alias
    JsonObject& aliasJson = root.createNestedObject("alias");
    aliasJson["value"] = alias;
    putVersionKey(aliasJson);

    // numLeds
    JsonObject& numLedsJson = root.createNestedObject("numLeds");
    numLedsJson["value"] = numLeds;
    putVersionKey(numLedsJson);

    // voltageScale
    JsonObject& voltageScaleJson = root.createNestedObject("voltageScale");
    voltageScaleJson["value"] = voltageScale;
    putVersionKey(voltageScaleJson);

    // warningCellVoltage
    JsonObject& warningCellVoltageJson = root.createNestedObject("warningCellVoltage");
    warningCellVoltageJson["value"] = warningCellVoltage;
    putVersionKey(warningCellVoltageJson);

    // brightness
    JsonObject& brightnessJson = root.createNestedObject("brightness");
    brightnessJson["value"] = brightness;
    putVersionKey(brightnessJson);

    #ifdef DEBUG_SPIFFS
        Serial.printf("saveSettingsConfig(): Saving settings config to settings.json\n");
        root.prettyPrintTo(Serial);
        Serial.println();
    #endif

    //Save to filesystem
    File configFile = SPIFFS.open("/settings.json", "w");
    if (!configFile) {
        Serial.println("saveSettingsConfig(): Failed to open config file for writing\n");
        return;
    }

    // Write JSON to file
    root.printTo(configFile);

    Serial.println("saveSettingsConfig(): Saved settings config\n");
}

/**
 * Add a version key with current software version
 */
void putVersionKey(JsonObject& obj) {
    JsonArray& jsonVersionArray = obj.createNestedArray("version");
    jsonVersionArray.copyFrom(version);
}

/**
 * Save the effects config to SPIFFS
 */
void saveEffectsConfig() {
    // Create Buffer
    DynamicJsonBuffer bufferEffects;
    // Create root object
    JsonObject& root = bufferEffects.createObject();

    // Put data inside

    // activeEffectId
    JsonObject& activeEffectIdJson = root.createNestedObject("activeEffectId");
    activeEffectIdJson["value"] = nextEffectId;
    putVersionKey(activeEffectIdJson);

    // Effects
    JsonArray& effectsJson = root.createNestedArray("effects");

    for(uint8_t i = 0; i < numEffects; i++) {
        // creat object
        JsonObject& obj = effectsJson.createNestedObject();
        // name
        obj["name"] = effects[i].name;
        // version
        putVersionKey(obj);
        // color
        JsonObject& colorJson = obj.createNestedObject("color");
        // primary color
        JsonArray& colorPrimaryJson = colorJson.createNestedArray("primary");
        colorPrimaryJson.add(effects[i].color[0].R);
        colorPrimaryJson.add(effects[i].color[0].G);
        colorPrimaryJson.add(effects[i].color[0].B);
        // secondary color
        JsonArray& colorSecondaryJson = colorJson.createNestedArray("secondary");
        colorSecondaryJson.add(effects[i].color[1].R);
        colorSecondaryJson.add(effects[i].color[1].G);
        colorSecondaryJson.add(effects[i].color[1].B);
        // para
        JsonArray& paraJson = obj.createNestedArray("para");
        paraJson.copyFrom(effects[i].para);
    }

    #ifdef DEBUG_SPIFFS
        Serial.printf("saveEffectsConfig(): Saving effects config to effects.json\n");
        root.prettyPrintTo(Serial);
        Serial.println();
    #endif

    //Save to filesystem
    
    File configFile = SPIFFS.open("/effects.json", "w");
    if (!configFile) {
        Serial.println("saveEffectsConfig(): Failed to open config file for writing\n");
        return;
    }
    
    // Write JSON to file
    root.printTo(configFile);

    Serial.println("saveEffectsConfig(): Saved effects config\n");  
}

/**
 * Fetch effects from user config files in SPIFFS or use defaults.
 */
void fetchEffectsConfig() {
    // Create Buffer
    DynamicJsonBuffer bufferEffectsDefault;
    // Try to parse default config, force duplicate of effectsDefaultJson
    JsonObject& rootEffectsDefault = bufferEffectsDefault.parseObject((const char*)effectsDefaultJson);
    // Check for parsing succeeds
    if(!rootEffectsDefault.success()) {
        Serial.printf("fetchEffectsConfig(): Parsing default root object failed, stop program\n");
        // endless loop, this error can't be corrected.
        while(true) {};
    }
    else {
        #ifdef DEBUG_SPIFFS
            Serial.printf("fetchEffectsConfig(): Parsing default root object ok\n");
            rootEffectsDefault.prettyPrintTo(Serial);
            Serial.println();
        #endif
    }

    boolean validUserFile = true;
    // Try loading user settings config file
    File effectsConfigFile = SPIFFS.open("/effects.json", "r");
    // check for error
    if(!effectsConfigFile) {
        Serial.printf("fetchEffectsConfig(): No user effects config found\n");
        validUserFile = false;
    } 
    else {
        size_t size = effectsConfigFile.size();
        if (size > 1024) {
            Serial.printf("fetchEffectsConfig(): Settings config file size is too large\n");
            validUserFile = false;
        }
    }
    // Try to parse JSON in user file
    size_t size = effectsConfigFile.size();
    // Allocate a buffer to store contents of the file.
    std::unique_ptr<char[]> effectsUserJson(new char[size]);
    // We don't use String here because ArduinoJson library requires the input
    // buffer to be mutable. If you don't use ArduinoJson, you may as well
    // use configFile.readString instead.
    effectsConfigFile.readBytes(effectsUserJson.get(), size);

    // Create Buffer
    DynamicJsonBuffer bufferEffectsUser;
    // Try to parse user config
    JsonObject& rootEffectsUser = bufferEffectsUser.parseObject(effectsUserJson.get());
    // Check for parsing succeeds
    if(!rootEffectsUser.success()) {
        Serial.printf("fetchEffectsConfig(): Parsing user root object failed, use defaults\n");
        validUserFile = false;
    }
    else {
        #ifdef DEBUG_SPIFFS
            Serial.printf("fetchEffectsConfig(): Parsing user root object ok\n");
            rootEffectsUser.prettyPrintTo(Serial);
            Serial.println();
        #endif
    }

    /**
     * ------ activeEffectId ------
     */
    uint8_t activeEffectIdUseDefault = true;
    if(validUserFile && rootEffectsUser.containsKey("activeEffectId")) {
        // check if version in config is greater or equal the needed version for this software version
        JsonObject& activeEffectIdUser = rootEffectsUser["activeEffectId"];
        JsonObject& activeEffectIdDefault = rootEffectsDefault["activeEffectId"];
        if(checkVersion(activeEffectIdUser, activeEffectIdDefault)) {
            activeEffectIdUseDefault = false;

            // Fetch real data
            nextEffectId = rootEffectsUser["activeEffectId"]["value"];
        }
    }
    if(activeEffectIdUseDefault) {
        // Fetch defaults
        nextEffectId = rootEffectsDefault["activeEffectId"]["value"];
    }

    /**
     * ------ effects ------
     */
    JsonArray& effectsArrayUser = rootEffectsUser["effects"];
    JsonArray& effectsArrayDefault = rootEffectsDefault["effects"];
    for(uint16_t i = 0; i < numEffects; i++) {
        uint8_t effectUseDefault = true;
        if(validUserFile && effectsArrayUser[i]) {
            // check if version in config is greater or equal the needed version for this software version
            JsonObject& effectUser = effectsArrayUser[i];
            JsonObject& effectDefault = effectsArrayDefault[i];
            if(checkVersion(effectUser, effectDefault)) {
                effectUseDefault = false;

                // Fetch real data

                // primary color
                JsonArray& primaryUser = effectsArrayUser[i]["color"]["primary"];
                effects[i].color[0] = RgbColor(primaryUser[0], primaryUser[1], primaryUser[2]);
                // secondary color
                JsonArray& secondaryUser = effectsArrayUser[i]["color"]["secondary"];
                effects[i].color[1] = RgbColor(secondaryUser[0], secondaryUser[1], secondaryUser[2]);
                // para
                JsonArray& paraUser = effectsArrayUser[i]["para"];
                paraUser.copyTo(effects[i].para);
            }
        }
        if(effectUseDefault) {
            // Fetch defaults

            // primary color
            JsonArray& primaryDefault = effectsArrayDefault[i]["color"]["primary"];
            effects[i].color[0] = RgbColor(primaryDefault[0], primaryDefault[1], primaryDefault[2]);
            // secondary color
            JsonArray& secondaryDefault = effectsArrayDefault[i]["color"]["secondary"];
            effects[i].color[1] = RgbColor(secondaryDefault[0], secondaryDefault[1], secondaryDefault[2]);
            // para
            JsonArray& paraDefault = effectsArrayDefault[i]["para"];
            paraDefault.copyTo(effects[i].para);
        }
    }

    Serial.printf("fetchEffectsConfig(): Settings loaded\n");
}

/**
 * Enable Task to save the config
 */
void enableSaveSettingsTask() {
    taskSaveSettingsConfig.setIterations(1);
    taskSaveSettingsConfig.enable();
}

/**
 * Enable Task to save the effects
 */
void enableSaveEffectsTask() {
    taskSaveEffectsConfig.setIterations(1);
    taskSaveEffectsConfig.enable();
}

/**
 * Load default colors for one effect
 */
void loadDefaultColor(uint16_t id) {
    // Create Buffer
    DynamicJsonBuffer bufferEffectsDefault;
    Serial.printf("%s", effectsDefaultJson);
    // Try to parse default config, force duplicate of effectsDefaultJson
    JsonObject& rootEffectsDefault = bufferEffectsDefault.parseObject((const char*)effectsDefaultJson);
    // Check for parsing succeeds
    if(!rootEffectsDefault.success()) {
        Serial.printf("loadDefaultColor(): Parsing default root object failed, stop program\n");
        // endless loop, this error can't be corrected.
        while(true) {};
    }
    else {
        #ifdef DEBUG_SPIFFS
            Serial.printf("loadDefaultColor(): Parsing default root object ok\n");
            rootEffectsDefault.prettyPrintTo(Serial);
            Serial.println();
        #endif
    }

    /**
     * ------ effects ------
     */
    JsonArray& effectsArrayDefault = rootEffectsDefault["effects"];
    // Fetch defaults
    // primary color
    JsonArray& primaryDefault = effectsArrayDefault[id]["color"]["primary"];
    effects[id].color[0] = RgbColor(primaryDefault[0], primaryDefault[1], primaryDefault[2]);
    // secondary color
    JsonArray& secondaryDefault = effectsArrayDefault[id]["color"]["secondary"];
    effects[id].color[1] = RgbColor(secondaryDefault[0], secondaryDefault[1], secondaryDefault[2]);

    Serial.printf("loadDefaultColor(): Settings loaded\n");
}

/**
 * Load default para for one effect
 */
void loadDefaultPara(uint16_t id) {
    // Create Buffer
    DynamicJsonBuffer bufferEffectsDefault;
    // Try to parse default config, force duplicate of effectsDefaultJson
    JsonObject& rootEffectsDefault = bufferEffectsDefault.parseObject((const char*)effectsDefaultJson);
    // Check for parsing succeeds
    if(!rootEffectsDefault.success()) {
        Serial.printf("loadDefaultPara(): Parsing default root object failed, stop program\n");
        // endless loop, this error can't be corrected.
        while(true) {};
    }
    else {
        #ifdef DEBUG_SPIFFS
            Serial.printf("loadDefaultPara(): Parsing default root object ok\n");
            rootEffectsDefault.prettyPrintTo(Serial);
            Serial.println();
        #endif
    }

    /**
     * ------ effects ------
     */
    JsonArray& effectsArrayDefault = rootEffectsDefault["effects"];
    // Fetch defaults
    // para
    JsonArray& paraDefault = effectsArrayDefault[id]["para"];
    paraDefault.copyTo(effects[id].para);


    Serial.printf("loadDefaultColor(): Settings loaded\n");
}

/**
 * Load default settings
 */
void loadDefaultSettings() {
    if(!SPIFFS.remove("/settings.json")) {
        Serial.printf("loadDefaultSettings(): Removing settings.json failed\n");
    }
    fetchSettingsConfig();
}

/*#######################################################
##                    End of file                      ##
#######################################################*/