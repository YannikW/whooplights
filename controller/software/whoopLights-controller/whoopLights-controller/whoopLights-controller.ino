/*
 * File:			whoopLights.ino
 * Project:			whoopLights
 * Created:			19.08.2018, 16:55:54
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	09.10.2018, 20:24:18
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/*#######################################################
##                      Includes                       ##
#######################################################*/

//Basic
#include "Arduino.h"

//External Libraries
#include "painlessMesh.h"
#include "NeoPixelBus.h"
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"
#include <DNSServer.h>

//User files / program separation
#include "config.h"
#include "defaults.h"
#include "MeshFunctions.h"
#include "Effects.h"
#include "Commands.h"
#include "Filesystem.h"


/*#######################################################
##                      Defines                        ##
#######################################################*/

const uint8_t version[] = {0,2,0};



/*#######################################################
##                      Objects                        ##
#######################################################*/

// Task scheduler for mesh and user tasks
Scheduler userScheduler;
// Mesh for synchronizing all nodes
painlessMesh mesh;
SimpleList<uint32_t> nodes;
// List of all known commands and handler functions
CommandListContainer cmdList;
// Command interpreter, uses cmdList
CommandParser meshParser("meshParser");
CommandParser wsParser("wsParser");
// Webserver and Websocketserver
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
// DNS server
DNSServer dnsServer;


/*#######################################################
##                       Tasks                         ##
#######################################################*/

//Task taskSendMessage(TASK_SECOND * 2, TASK_FOREVER, &sendMessage);
Task taskReadVoltage(TASK_SECOND * VOLTAGE_READ_DELAY, TASK_FOREVER, &readVoltage);
// See Effects.cpp for warning()
Task taskWarning(500, TASK_FOREVER, &warning);
// Save Settings
/**
 * This is done in a task because calling a methode which creates a JsonBuffer
 * from a methode where a jsonBuffer exists crashed often (because variabled get inherited)
 */
Task taskSaveSettingsConfig(100, 1, &saveSettingsConfig);
Task taskSaveEffectsConfig(100, 1, &saveEffectsConfig);


/*#######################################################
##                     Variables                       ##
#######################################################*/

// settings.json
char alias[64];
uint16_t numLeds;
uint16_t voltageScale;
uint16_t warningCellVoltage;
uint8_t brightness;

// Voltage
uint16_t voltage;
uint8_t voltageUnderWarningCount = 0;
uint8_t cells = 0;

// Externals
extern effect_t effects[numEffects];



/*#######################################################
##                       Setup                         ##
#######################################################*/

void setup() {
    // Serial for Debug
    Serial.begin(115200);
    Serial.printf("\n\n\n");
    // Wait some time for serial monitor to open
    delay(200);

    // Init Mesh
    mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);
    mesh.onReceive(&receivedCallback);
    mesh.onNewConnection(&newConnectionCallback);
    mesh.onChangedConnections(&changedConnectionCallback);
    mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
    mesh.onNodeDelayReceived(&delayReceivedCallback);

    // Fetch config files (or load defaults)
    initSPIFFS();
    //SPIFFS.format();
    fetchSettingsConfig();
    saveSettingsConfig();
    fetchEffectsConfig();
    saveEffectsConfig();

    // Init Tasks
    //userScheduler.addTask(taskSendMessage);
    //taskSendMessage.enable();
    userScheduler.addTask(taskReadVoltage);
    taskReadVoltage.enableDelayed(1000);
    userScheduler.addTask(taskSaveEffectsConfig);
    userScheduler.addTask(taskSaveSettingsConfig);

    // Init Leds
    initLeds(numLeds);

    // Init Effects
    initializeEffects();

    //Initialize command dictionary and parser
    initCommands();
    initParser();

    // attach AsyncWebSocket
    ws.onEvent(onEvent);
    server.addHandler(&ws);
    server.onNotFound(onRequest);
    server.onFileUpload(onUpload);
    server.onRequestBody(onBody);
    server.begin();

    // DNS server
    dnsServer.setErrorReplyCode(DNSReplyCode::ServerFailure);
    dnsServer.start(53, DNS_ADDRESS, WiFi.softAPIP());
    Serial.printf("setup(): AP IP: %u.%u.%u.%u\n", WiFi.softAPIP()[0], WiFi.softAPIP()[1], WiFi.softAPIP()[2], WiFi.softAPIP()[3]);

    // Setup finished
    Serial.printf("setup(): My mesh id: %u\n", mesh.getNodeId());
    Serial.println("setup(): completed");
}


/*#######################################################
##                        Loop                         ##
#######################################################*/

void loop() {
    // Update scheduler, mesh and parser here - do nothing else!
    userScheduler.execute();
    mesh.update();
    dnsServer.processNextRequest();
}

/*#######################################################
##                      Functions                      ##
#######################################################*/

void sendMessage() {
    String msg = "Hello from node ";
    msg += mesh.getNodeId();
    mesh.sendBroadcast(msg);

    Serial.printf("Sending message: %s\n", msg.c_str());
}

void helloNew(JsonObject& data) {
    Serial.printf("helloNew(): Hi\n");
}


void onRequest(AsyncWebServerRequest *request){
    Serial.printf("onRequest(): \n");
    request->send(404);
}

void onBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total){
    Serial.printf("onBody(): \n");
}

void onUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final){
    Serial.printf("onUpload(): \n");
}

void onEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
    if(type == WS_EVT_CONNECT){
    /*
    //client connected
    Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
    client->printf("Hello Client %u :)", client->id());
    client->ping();
    */
    } else if(type == WS_EVT_DISCONNECT){
    /*
    //client disconnected
    Serial.printf("ws[%s][%u] disconnect: %u\n", server->url(), client->id());
    */
    } else if(type == WS_EVT_ERROR){
    /*
    //error was received from the other end
    Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
    */
    } else if(type == WS_EVT_PONG){
    /*
    //pong message was received (in response to a ping request maybe)
    Serial.printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len)?(char*)data:"");
    */
    } else if(type == WS_EVT_DATA){
        data[len] = 0;
        // Parse message
        wsParser.parse((char*)data);

        //Forward message to mesh
        String str((char*)data);
        mesh.sendBroadcast(str);
    }
}

/**
 * Reading battery voltage.
 */
void readVoltage() {
    voltage = ((uint32_t)analogRead(A0) * voltageScale) / 1000;

    #ifdef PRINT_VOLTAGE
        Serial.printf("readVoltage(): Voltage is %umV\n", voltage);
    #endif

    // first time this executed we will detect cells
    if(cells == 0) {
        cells = (voltage / CELL_THRESHOLD) + 1;
        Serial.printf("readVoltage(): Cells detected: %u, Voltage: %umV\n", cells, voltage);
    }

    // check for warning voltage
    if(voltage < (warningCellVoltage*cells)) {
        voltageUnderWarningCount++;
        if(voltageUnderWarningCount >= NUM_VOLTAGE_UNDER_WARNING) {
            // disable effects only first time we have undervoltage warning
            // it will run forever anyways
            if(voltageUnderWarningCount == NUM_VOLTAGE_UNDER_WARNING) {
                // stop led task and start red blink task
                disableEffects();
                taskWarning.enable();
            }
            Serial.printf("readVoltage(): WARNING! Voltage low: %umV\n", voltage);
        }
    }
    else {
        voltageUnderWarningCount = 0;
    }

    // calcBrightness
    calcBrightness(voltage);
}