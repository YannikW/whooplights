/*
 * File:			Filesystem.h
 * Project:			whoopLights
 * Created:			22.09.2018, 19:21:29
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	07.10.2018, 11:17:25
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef Filesystem_h
#define Filesystem_h

/*#######################################################
##                      Includes                       ##
#######################################################*/

//Basic
#include "Arduino.h"

//External Libraries
#include "FS.h"

//Internal Libraries

//User files / program separation
#include "Effects.h"


/*#######################################################
##                     Constants                       ##
#######################################################*/


/*#######################################################
##                     Externals                       ##
#######################################################*/

extern const uint8_t version[3];

extern char *settingsDefaultJson;
extern char *effectsDefaultJson;

// settings.json
extern char alias[64];
extern uint16_t numLeds;
extern uint16_t voltageScale;
extern uint16_t warningCellVoltage;
extern uint8_t brightness;

// effects.json
extern effect_t effects[numEffects];
extern uint16_t nextEffectId;

extern Task taskSaveSettingsConfig;
extern Task taskSaveEffectsConfig;


/*#######################################################
##                 Function prototypes                 ##
#######################################################*/

void initSPIFFS();
void fetchSettingsConfig();
boolean checkVersion(JsonObject& first, JsonObject& second);
boolean isVersionGreaterOrEqual(uint8_t *myVersion, uint8_t *checkVersion);
void saveSettingsConfig();
void putVersionKey(JsonObject& obj);
void saveEffectsConfig();
void fetchEffectsConfig();
void enableSaveSettingsTask();
void enableSaveEffectsTask();
void loadDefaultColor(uint16_t id);
void loadDefaultPara(uint16_t id);
void loadDefaultSettings();



/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif  // Filesystem_h