/*
 * File:			Effects.h
 * Project:			whoopLights
 * Created:			19.08.2018, 16:55:54
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	08.10.2018, 17:59:01
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef _EFFECTS_H
#define _EFFECTS_H

/*#######################################################
##                      Includes                       ##
#######################################################*/

//Basic
#include "Arduino.h"

//External Libraries
#include "NeoPixelBrightnessBus.h"
#include "painlessMesh.h"
#include "ESP8266TrueRandom.h"

//User files / program separation
#include "config.h"
#include "utils.h"


/*#######################################################
##                     Constants                       ##
#######################################################*/

//struct which defines a parameter set for all effects
struct effect_t {
    String name;            // name
    RgbColor color[2];      // primary and secondary color
    uint32_t para[5];       // parameters (max 5)
};

struct effectCallback_t {
    String name;            // name
    void (*function)();     // callback function
};


/*#######################################################
##                     Externals                       ##
#######################################################*/

extern Scheduler userScheduler;
extern painlessMesh mesh;
extern Task taskWarning;

extern uint8_t brightness;

/*#######################################################
##                 Function prototypes                 ##
#######################################################*/

void initializeEffects();
void initLeds(uint8_t num);
void changePixelCount(uint16_t n);
void enableEffect(uint16_t extIterations=1);
void setNextEffect(uint16_t id);
void disableEffects();
void calcBrightness(uint16_t voltage);
void setStripColor(RgbColor color, boolean show);
void warning();
void setStripColorFromTo(RgbColor color, uint16_t from, uint16_t to, boolean show);

//Effects
void rainbow();
RgbColor wheel(uint8_t wheelPos);
void fire();
void tunnel();
void larsonScanner();
void snake();
void theaterChase();
void strobe();
void police();


/*#######################################################
##              Default effect parameters              ##
#######################################################*/

const effectCallback_t effectCallbacks[] = {
/** 0: Rainbow
 *  
 * primaryColor:    -
 * secondaryColor:  -
 * 
 * para[0]: Delay in ms between one step in calculation (slower values means faster moving of the rainbow)
 * para[1]: Number of complete rainbows from beginning to end of the strip
 * para[2]: Direction (0 = normal, 1 = reverse)
 * para[3]: -
 * para[4]: -
 */
//   id,    name,              primary Color,              secondary Color,            para[0],    para[1],    para[2],    para[3],    para[4],     function,       iterations
    {"Rainbow", rainbow},
 /** 1: Fire
 *  
 * primaryColor:    Fire color
 * secondaryColor:  -
 * 
 * para[0]: Fixed delay between two calculations
 * para[1]: Random delay between two calculations. (Total delay = fixed delay + random delay)
 * para[2]: Flicker value
 * para[3]: -
 * para[4]: -
 */
//   id,    name,              primary Color,              secondary Color,            para[0],    para[1],    para[2],    para[3],    para[4],     function,       iterations
    {"Fire", fire},

    {"Tunnel", tunnel},
    {"Larson Scanner", larsonScanner},
    {"Snake", snake},
    {"Theater Chase", theaterChase},
    {"Strobe", strobe},
    {"Police", police}
};
static const size_t numEffects = sizeof(effectCallbacks)/sizeof(effectCallbacks[0]);


/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif //_EFFECTS_H