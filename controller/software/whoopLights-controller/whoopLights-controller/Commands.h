/*
 * File:			Commands.h
 * Project:			whoopLights
 * Created:			13.09.2018, 20:33:38
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	05.10.2018, 18:42:46
 * Modified By:		Yannik Wertz
 * -----
 * whoopLights is a network synchronized LED effect controller for FPV racetracks.
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef Commands_h
#define Commands_h

/*#######################################################
##                      Includes                       ##
#######################################################*/

//Basic
#include "Arduino.h"

//External Libraries
#include "painlessMesh.h"
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"

//Internal Libraries
#include "CommandParser.h"

//User files / program separation
#include "config.h"
#include "Effects.h"
#include "Filesystem.h"


/*#######################################################
##                     Constants                       ##
#######################################################*/

/*#######################################################
##                     Externals                       ##
#######################################################*/

extern painlessMesh mesh;
extern CommandListContainer cmdList;
extern CommandParser meshParser;
extern CommandParser wsParser;
extern AsyncWebSocket ws;

extern char alias[64];
extern effect_t effects[numEffects];
extern uint16_t activeEffectId;
extern uint16_t nextEffectId;
extern uint16_t voltage;

extern void readVoltage();

/*#######################################################
##                 Function prototypes                 ##
#######################################################*/

// Initialization
void initCommands();
void initParser();

// Basic
void sendCommand(JsonObject& root, uint8_t type, const char *cmd, uint32_t target=0);
 
// Command callbacks
void getPara(JsonObject& data);
void getSettings(JsonObject& data);
void getStatus(JsonObject& data);
void resetColor(JsonObject& data);
void resetPara(JsonObject& data);
void resetSettings(JsonObject& data);
void setAlias(JsonObject& data);
void setColor(JsonObject& data);
void setEffect(JsonObject& data);
void setPara(JsonObject& data);
void setSettings(JsonObject& data);

// Client -> Master Commands
void para(uint32_t target);
void settings(uint32_t target);
void status(uint32_t target);


/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif  // Commands_h