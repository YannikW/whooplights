/*
 * File:			CommandParser.cpp
 * Project:			CommandParser
 * Created:			12.09.2018, 09:40:10
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	23.09.2018, 20:19:52
 * Modified By:		Yannik Wertz
 * -----
 * CommandParser is a library to parse strings with a command and multiple arguments.
 * User have to provide these strings to the library manually, so you can use multiple
 * sources (serial, websocket, ...).
 * Optional there can be a id before the command. The command will only be executed
 * if the id matches the own id or if the message is a broadcast (id=0).
 *  * -----
 * The library is based on Arduino-SerialCommand by kroimon,
 * see: https://github.com/kroimon/Arduino-SerialCommand
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#include "CommandParser.h"

/**
 * Constructor without special debug name
 */
CommandParser::CommandParser() {
    init("CommandParser");
}

/**
 * Constructor with user defined debug name
 */
CommandParser::CommandParser(const char *name) {
    init(name);
}

/**
 * Constructor with user defined debug name
 */
CommandParser::CommandParser(String& name) {
    init(name.c_str());
}

/**
 * Called only by constructor!
 * Initialize values.
 */
void CommandParser::init(const char *name) {
    cmdListCont = NULL;
    bufPos = 0;
    squareCount = 0;
    myId = 0;
    strcpy(debugName, name);
}

/**
 * Assign a command list dictionary to the parser
 */
void CommandParser::addCommandList(CommandListContainer &_cmdListContainer) {
    cmdListCont = &_cmdListContainer;
}


/**
 * Set id of this node.
 * Only messages which matches this id will be executed
 */
void CommandParser::setId(uint32_t id) {
    #ifdef COMMANDPARSER_DEBUG
            Serial.printf("%s::setId(): Set id to: %u\n", debugName, id);
    #endif
    myId = id;
}

/**
 * This is called to parse a char array.
 * It will be added to the buffer.
 * When a complate JSON string is detected (number of "{" equals the number of "}"),
 * the JSON string will be parsed and the handler function, defined in the CommandList,
 * will be called.
 * Notice: Char array needs to have a null-character at the end.
 * 
 * Return:
 *  -1: Error, invalid JSON string or buffer is full without finding a complete JSON string or invalid start char
 *   0: Char(s) are added to the buffer, but no complete JSON string is detected
 *   1: Complete JSON string detected and parsed without errors.
 *      If the message ist TYPE_TARGET, this does not mean the message is targeted to this node.
 *      If it's targeted to another node it will return success either.
 *      But handler function will, of cause, not be called in this case.
 */
int8_t CommandParser::parse(char *cStr) {
    // Loop through all characters until the null-character is found
    for(uint16_t cIdx = 0; cIdx < strlen(cStr); cIdx++) {
        char c = cStr[cIdx];    // Get a singe char from the char array
        // Only process printable characters (JSON string must not have non printable characters or this library will fail)
        if(isprint(c)) {
            // Empty buffer means a new JSON string should start and first char has to be opening square bracket 
            if(bufPos == 0 && c != '{') {
                 #ifdef COMMANDPARSER_DEBUG
                    Serial.printf("%s::parse(): Expected '{' but got %c\n", debugName, c);
                #endif
                clearBuffer();
                return -1;
            }
            // Check for square brackets to find end of json string
            if(c == '{') squareCount++;
            else if (c == '}') squareCount--;

            // Put char in the buffer
            if (bufPos < COMMANDPARSER_BUFFER) {
                buffer[bufPos++] = c;  // Put character into buffer
                buffer[bufPos] = '\0'; // Null terminate
            } 
            else {
                #ifdef COMMANDPARSER_DEBUG
                    Serial.printf("%s::parse(): Line buffer is full - increase COMMANDPARSER_BUFFER\n", debugName);
                #endif
                clearBuffer();
                return -1;  // Buffer is full and no completed JSON string has been found
            }

            // Check if JSON string is completed
            if(squareCount == 0) {
                #ifdef COMMANDPARSER_DEBUG
                    Serial.printf("%s::parse(): Received JSON string: \"%s\"\n", debugName, buffer);
                #endif
                
                // Create a buffer for ArduinoJson und try to parse the JSON string
                DynamicJsonBuffer jsonBuffer;
                JsonObject& root = jsonBuffer.parseObject(buffer);

                // Check for parsing succeeds
                if(!root.success()) {
                    #ifdef COMMANDPARSER_DEBUG
                        Serial.printf("%s::parse(): Parsing root object failed\n", debugName);
                    #endif
                    clearBuffer();
                    return -1;
                }

                // Get message type, command and data
                uint8_t type = root["type"];
                const char *command = root["command"];
                JsonObject& data = root["data"];
                uint32_t source = root["source"];
                uint32_t target;    // not always needed, but can't be created inside switch case

                data["source"] = source;    // Add source to data object to pass it to the handler function

                if(!command) {
                    #ifdef COMMANDPARSER_DEBUG
                        Serial.printf("%s::parse(): Missing command\n", debugName);
                    #endif
                    return -1;
                }

                switch (type) {
                    case TYPE_BROADCAST:
                        callHandlerFunction(command, data);
                        break;

                    case TYPE_TARGET:
                        target = root["target"];
                        if(target == myId) {
                            callHandlerFunction(command, data);
                        }
                        else {
                            #ifdef COMMANDPARSER_DEBUG
                                Serial.printf("%s::parse(): Received message but I'm not the target\n", debugName);
                            #endif
                        }
                        break;

                    default:
                        #ifdef COMMANDPARSER_DEBUG
                            Serial.printf("%s::parse(): Unknown or missing \"type\" key\n", debugName);
                        #endif
                        clearBuffer();
                        return -1;
                }
                
                clearBuffer();
                return 1;   // Successfully parsed the JSON string
            }
        }
    }
    return 0;   // Char (array) was added to the buffer without error, but JSON string isn't completed yet
}

/**
 * parse() for single char
 */
int8_t CommandParser::parse(char c) {
        char inp[2];
        inp[0] = c;
        inp[1] = '\0';
        return parse(inp);
}

/**
 * Set new name for debugging output (usefull if you have multiple instances)
 */
void CommandParser::setDebugName(const char *name) {
    strcpy(debugName, name);
}

/**
 * Clear the input buffer.
 */
void CommandParser::clearBuffer() {
  buffer[0] = '\0';
  bufPos = 0;
  squareCount = 0;
}

/**
 * Compare command with the commandList.
 * If a match is found call the function,
 * if not call the default handler (id set).
 */
void CommandParser::callHandlerFunction(const char *command, JsonObject& data) {
    boolean matched = false;
    // check if there is a command dictionary
    if(cmdListCont != NULL) {
        for (int i = 0; i < cmdListCont->commandCount; i++) {
            #ifdef COMMANDPARSER_DEBUG
                Serial.printf("%s::parse(): Comparing [%s] to [%s]\n", debugName, command, cmdListCont->commandList[i].command);
            #endif

            // Compare the found command against the list of known commands for a match
            if (strncmp(command, cmdListCont->commandList[i].command, COMMANDLIST_MAXCOMMANDLENGTH) == 0) {
                #ifdef COMMANDPARSER_DEBUG
                    Serial.printf("%s::parse(): Matched command: %s\n", debugName, command);
                #endif

                // Execute the stored handler function for the command
                cmdListCont->commandList[i].function(data);
                matched = true;
                break;
            }
        }

        if (!matched && (cmdListCont->defaultHandler != NULL)) {
            cmdListCont->defaultHandler(command);
        }
    }
    else {
        #ifdef COMMANDPARSER_DEBUG
            Serial.printf("%s::parse(): No command dictionary found\n", debugName);
        #endif
    }
}

/**
 * Constructor makes sure some things are set.
 */
CommandListContainer::CommandListContainer()
  : commandList(NULL),
    commandCount(0),
    defaultHandler(NULL)
{
    // Do nothing here
}

/**
 * Adds a "command" and a handler function to the list of available commands.
 * This is used for matching a found token in the buffer, and gives the pointer
 * to the handler function to deal with it.
 */
void CommandListContainer::addCommand(const char *command, void (*function)(JsonObject&)) {
    #ifdef COMMANDLIST_DEBUG
        Serial.printf("CommandListContainer::addCommand(): Adding command (%u): %s\n", commandCount, command);
    #endif

    // resize commandList array
    commandList = (CommandListCallback *) realloc(commandList, (commandCount + 1) * sizeof(CommandListCallback));
    // add cmd
    strncpy(commandList[commandCount].command, command, COMMANDLIST_MAXCOMMANDLENGTH);
    commandList[commandCount].function = function;
    commandCount++;
}

/**
 * This sets up a handler to be called in the event that the receveived command string
 * isn't in the list of commands.
 */
void CommandListContainer::setDefaultHandler(void (*function)(const char *)) {
    defaultHandler = function;
}


/*#######################################################
##                    End of file                      ##
#######################################################*/