/*
 * File:			CommandParser.h
 * Project:			CommandParser
 * Created:			12.09.2018, 10:27:47
 * Author:			Yannik Wertz
 * -----
 * Last Modified:	19.09.2018, 09:51:54
 * Modified By:		Yannik Wertz
 * -----
 * CommandParser is a library to parse strings with a command and multiple arguments.
 * User have to provide these strings to the library manually, so you can use multiple
 * sources (serial, websocket, ...).
 * Optional there can be a id before the command. The command will only be executed
 * if the id matches the own id or if the message is a broadcast (id=0).
 * -----
 * The library is based on Arduino-SerialCommand by kroimon,
 * see: https://github.com/kroimon/Arduino-SerialCommand
 * -----
 * Copyright (c) 2018 Yannik Wertz
 * -----
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CommandParser_h
#define CommandParser_h

/*#######################################################
##                      Includes                       ##
#######################################################*/

// Basic
#include "Arduino.h"

//External Libraries
#include "ArduinoJson.h"


/*#######################################################
##                      Defines                        ##
#######################################################*/

// Size of the input buffer in bytes (maximum length of JSON string)
#define COMMANDPARSER_BUFFER 512
// Maximum length of a command excluding the terminating null
#define COMMANDLIST_MAXCOMMANDLENGTH 32
/**
 * Size of JSON buffer object, needed size can be calculated here: https://arduinojson.org/v5/assistant/
 * Atmel Chips don't have much RAM (only 2k for Atmega328) so 1024 (Byte) is very big and not recommended.
 * For ESP8266 or ESP32 RAM is not an issue (about 50kb free with basic sketch), so 1k is no problem here
 */
#define JSONBUFFER_SIZE 1024

// Uncomment the next line(s) to run the library in debug mode (verbose messages)
#define COMMANDPARSER_DEBUG
//#define COMMANDLIST_DEBUG

/**
 * !! Do not change something beyond this line unless you know wht you're doing !!
 * ----------------------------------------------------------------------------------------------------------
 */

// Message types
#define TYPE_BROADCAST  1
#define TYPE_TARGET     2 


/*#######################################################
##                       Class                         ##
#######################################################*/

class CommandListContainer; // prototype definitions for CommandParser class

class CommandParser {
    public:
        CommandParser();    // Constructor
        CommandParser(const char *name);
        CommandParser(String& name);
        void addCommandList(CommandListContainer &_cmdListCont);
        void setId(uint32_t id);
        int8_t parse(char *c); // Main entry point.
        int8_t parse(char c);
        void setDebugName(const char *name);
        

    private:
        void init(const char *name);
        void clearBuffer(); // Clears the input buffer.
        void callHandlerFunction(const char *command, JsonObject& data);

        CommandListContainer *cmdListCont;      // A list of all commands and handler functions
        char buffer[COMMANDPARSER_BUFFER + 1];  // Buffer of stored characters while waiting for terminator character
        uint16_t bufPos;                        // Current position in the buffer
        int8_t squareCount; // '{' will add 1 and '}' will substract 1. When is 0 (after it was >0) JSON string is completed
        uint32_t myId;  // Id to compare with messages
        char debugName[32];
};

class CommandListContainer {
    public:
        CommandListContainer(); // Constructor
        void addCommand(const char *command, void(*function)(JsonObject&));  // Add a command to the processing dictionary.
        void setDefaultHandler(void (*function)(const char *));   // A handler to call when no valid command received.

        // Command/handler dictionary
        struct CommandListCallback {
        char command[COMMANDLIST_MAXCOMMANDLENGTH + 1];
        void (*function)(JsonObject&);
        };                                  // Data structure to hold Command/Handler function key-value pairs
        CommandListCallback *commandList;   // Actual definition for command/handler array
        byte commandCount;

        // Pointer to the default handler function
        void (*defaultHandler)(const char *);
};


/*#######################################################
##                    End of file                      ##
#######################################################*/

#endif // CommandParser_h